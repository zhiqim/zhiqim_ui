/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z)
{
//BEGIN

/**
 * mobile 端图片裁切
*/
Z.ImageClipper = Z.Class.newInstance();
Z.ImageClipper.prototype = 
{
    defaults:
    {
        elem : null,
        ratio: 1,
        state : {},
        img: null,
        clipWidth: [50, 100, 150],
        save: null
    },
    
    execute: function()
    {
        this.$elem = Z.$elem(this.elem, "Z.Floater");
        
        if (this.clipWidth == null || this.clipWidth.length == 0)
        {
            Z.alert("[Z.ImageClipper]没有设置clipWidth，或不是数组");
            return;
        }
    
        this.id = Z.random(10);
        
        //判断是不是手机端
        this.width = this.$elem.offsetWidth();
        var html = '<div id="ZImageClipper_'+this.id+'" class="z-relative z-w100p">'
                 + '    <div id="ZImageClipper_image_'+this.id+'" class="z-relative z-w100p z-bd z-overflow-hidden z-bg-white" style="cursor:move;background-repeat:no-repeat;height:'+this.width+'px;">'
                 + '        <div id="ZImageClipper_square_'+this.id+'" class="z-absolute z-w50p z-h50p z-bd" style="top:25%;left:25%;box-shadow:0 0 0 1000px rgba(0, 0, 0, 0.5);"></div>'
                 + '        <div id="ZImageClipper_loading_'+this.id+'" class="z-absolute-center-middle z-w60 z-h30 z-hide">加载中...</div>'
                 + '        <div id="ZImageClipper_clipped_'+this.id+'" class="z-absolute z-w100p z-l0 z-t0 z-text-center z-pd-t20 z-bd z-hide" style="height:'+this.width+'px;"></div>'
                 + '    </div>'
                 + '    <div class="z-button-row z-w100p z-mg-t10">'
                 + '        <button type="button" class="z-button-flex z-h50 zi-rem14" id="ZImageClipper_upload_'+this.id+'">上传图像 </button>'
                 + '        <button type="button" class="z-button-flex z-h50 zi-rem16" id="ZImageClipper_zoomIn_'+this.id+'">+</button>'
                 + '        <button type="button" class="z-button-flex z-h50 zi-rem16" id="ZImageClipper_zoomOut_'+this.id+'">-</button>'
                 + '        <button type="button" class="z-button-flex z-h50 zi-rem14" id="ZImageClipper_save_'+this.id+'">确定</button>'
                 + '    </div>'
                 + '</div>';
                 
        this.$elem.html(html);
        this.$imageBox = this.$elem.find("#ZImageClipper_image_"+this.id);
        this.$square =  this.$elem.find("#ZImageClipper_square_"+this.id);
        this.$loading = this.$elem.find("#ZImageClipper_loading_"+this.id).show();
        this.$$button = this.$elem.find("button");
        
        this.image = new Image();
        Z(this.image).load(function() 
        {
            this.$loading.hide();
            this.setBackground();
            
            this.$imageBox.on("touchstart", this.onTouchStart, this)
                .on("touchmove", this.onTouchMove, this)
                .on("mouseup mouseleave", this.onTouchEnd, this);
        }, this);
        this.image.src = this.img;
    
        Z("#ZImageClipper_zoomIn_"+this.id).on("touchstart", this.onZoomIn, this);
        Z("#ZImageClipper_zoomOut_"+this.id).on("touchstart", this.onZoomOut, this);
    
        this.$file = Z("<input id='ZImageClipper_upload_file_"+this.id+"' type='file' accept='image/jpg,image/jpeg,image/png' class='z-hide' single>");
        this.$file.appendTo("body").change(function()
        {
            var file = this.$file[0].files[0];
            var reader = new FileReader();
            reader.onload = Z.bind(function(e)
            {
                this.img = e.target.result;
                this.image.src = this.img;
            }, this);

            reader.readAsDataURL(file);
        }, this);
        
        Z("#ZImageClipper_upload_"+this.id).on("touchstart",function(){this.$file[0].click();}, this);
                
        if (Z.T.isFunction(this.save)){
            Z("#ZImageClipper_save_"+this.id).on("touchstart", this.save, this);
        }
    },
    
    setBackground: function()
    {
        var w =  parseInt(this.image.width) * this.ratio;
        var h =  parseInt(this.image.height) * this.ratio;

        var pw = (400 - w) / 2;
        var ph = (400 - h) / 2;

        this.$imageBox.css({
            "background-image": "url(" + this.image.src + ")",
            "background-size": w +"px " + h + "px",
            "background-position": pw + "px " + ph + "px",
            "background-repeat": "no-repeat"});
    },
    
    onTouchStart: function(e)
    {
        Z.E.forbidden(e);
        this.state.dragging = true;
        var theTouch = e.touches[0];
        this.state.mouseX = theTouch.clientX;
        this.state.mouseY = theTouch.clientY;
    },
    
    onTouchMove: function(e)
    {
        Z.E.forbidden(e);
        if (!this.state.dragging)
            return;
        
        var theTouch = e.touches[0];
        var x = theTouch.clientX - this.state.mouseX;
        var y = theTouch.clientY - this.state.mouseY;
        
        var bg = this.$imageBox.css('background-position').split(' ');
        
        var bgX = x + parseInt(bg[0]);
        var bgY = y + parseInt(bg[1]);
        
        this.$imageBox.css('background-position', bgX +'px ' + bgY + 'px');
        
        this.state.mouseX = theTouch.clientX;
        this.state.mouseY = theTouch.clientY;
    },
    
    onTouchEnd: function(e)
    {
        Z.E.forbidden(e);
        this.state.dragging = false;
    },
    
    onZoomIn: function()
    {
        this.ratio *= 1.1;
        this.setBackground();
    },
    
    onZoomOut: function()
    {
        this.ratio *= 0.9;
        this.setBackground();
    },
    
    getDataURL: function()
    {
        var width = this.$square.offsetWidth(),
            height = this.$square.offsetHeight(),
            canvas = document.createElement("canvas"),
            dim = this.$imageBox.css('background-position').split(' '),
            size = this.$imageBox.css('background-size').split(' '),
            dx = parseInt(dim[0]) - this.$imageBox.offsetWidth()/2 + width/2,
            dy = parseInt(dim[1]) - this.$imageBox.offsetHeight()/2 + height/2,
            dw = parseInt(size[0]),
            dh = parseInt(size[1]),
            sh = parseInt(this.image.height),
            sw = parseInt(this.image.width);

        canvas.width = width;
        canvas.height = height;
        var context = canvas.getContext("2d");
        context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);
        var imageData = canvas.toDataURL('image/png');
        return imageData;
    },
    
    getBlob: function()
    {
        var imageData = this.getDataURL();
        var b64 = imageData.replace('data:image/png;base64,','');
        var binary = atob(b64);
        var array = [];
        for (var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/png'});
    }
}

//END
})(zhiqim);