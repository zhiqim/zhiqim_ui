/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z)
{
//BEGIN
/************************************************************************************************/
//滚屏，每次滚轮滚动一个屏幕高度，并增加上下两个按钮点击切换屏幕，支持指定固定顶部高度和滚动速度
/************************************************************************************************/
//var sScreen = new Z.ScrollScreen();
//sScreen.fixHeight = 55;
//sScreen.execute();

Z.ScrollScreen = Z.Class.newInstance();
Z.ScrollScreen.prototype = 
{
    defaults:
    {
        speed: 13,
        times: 13,
        fixHeight:0,
        zIndex: 0
    },
    
    execute: function()
    {
        //支持滚屏的对象
        this.$screen = Z("[data-role=z-screen]");
    
        //监听初始化事件、窗口变化事件和滚轮事件
        if (!Z.B.firefox)
        {//非火狐刷新时执行window.scrollTo(0, 0)后会被浏览器跳转到原来位置，所以预先scrollTo(0, 0)
            Z(window).beforeunload(this.onLoad, this);
        }
        Z(window).load(this.onLoad, this).resize(this.onResize, this);
        Z(document).mousewheel(this.onMouseWheel, this);
                
        //向上按钮
        this.$up = Z('<div class="z-fixed z-w30 z-h30 z-bd-rd50p z-pointer"><i class="z-arrow z-up z-px5 z-absolute" style="margin:auto;top:0;left:0;bottom:0;right:0;"><span style="border-bottom-color:#d3d3d3"></span></i></div>');
        this.$up.appendTo("body").css({top: this.fixHeight+20, right: 20}).css({backgroundColor: "#d3d3d3", zIndex: this.zIndex})
            .click(this.onUp, this).mouseover(this.onUpMouseOver, this).mouseout(this.onUpMouseOut, this);
        
        //向下按钮
        this.$down = Z('<div class="z-fixed z-w30 z-h30 z-bd-rd50p z-pointer"><i class="z-arrow z-px5 z-absolute" style="margin:auto;top:0;left:0;bottom:0;right:0;"><span style="border-top-color:#d3d3d3"></span></i></div>');
        this.$down.appendTo("body").css({bottom: 20, right: 20}).css({backgroundColor: "#d3d3d3", zIndex: this.zIndex})
            .click(this.onDown, this).mouseover(this.onDownMouseOver, this).mouseout(this.onDownMouseOut, this);
            
        //初始化高度和设置滚屏高度
        this.onResize();
    },
    
    onLoad: function(e)
    {//刷新时回最上面
        window.scrollTo(0, 0);
    },
        
    onResize: function(e)
    {//重新设置高度和滚屏高度
        window.scrollTo(0, 0);
        this.running = false;
        this.height = Z.D.clientHeight() - this.fixHeight;
        this.$screen.css("height", this.height);
        this.maxHeight = Z.D.scrollHeight() - Z.D.clientHeight();
        this.$up.hide();
    },
    
    onUpMouseOver: function(e)
    {
        this.$up.css("backgroundColor", "#333")
            .find("i").css("borderBottomColor", "#fff")
            .find("span").css("borderBottomColor", "#333");
    },
    
    onUpMouseOut: function(e)
    {
        this.$up.css("backgroundColor", "#d3d3d3")
            .find("i").css("borderBottomColor", "#000")
            .find("span").css("borderBottomColor", "#d3d3d3");
    },
    
    onDownMouseOver: function(e)
    {
        this.$down.css("backgroundColor", "#333")
            .find("i").css("borderTopColor", "#fff")
            .find("span").css("borderTopColor", "#333");
    },
    
    onDownMouseOut: function(e)
    {
        this.$down.css("backgroundColor", "#d3d3d3")
            .find("i").css("borderTopColor", "#000")
            .find("span").css("borderTopColor", "#d3d3d3");
    },
    
    onUp: function()
    {//点击向上按钮
        if (this.running)
            return;
            
        this.running = true; 
        this.scrollTo(false);
    },
    
    onDown: function()
    {//点击向下按钮
        if (this.running)
            return;
            
        this.running = true; 
        this.scrollTo(true);
    },
    
    onMouseWheel: function(e)
    {//滚动滚轮
        Z.E.forbidden(e);
        if (this.running)
            return;
            
        this.running = true;
        this.scrollTo(Z.E.wheelDelta(e) < 0);
    },
    
    scrollTo: function(down)
    {//滚动操作
        var curHeight = Z.D.scrollTop();
        
        var index = 1;var endHeight, midHeight;
        if (down)
        {//向下
            if (this.maxHeight <= curHeight){
                this.running = false;
                return;
            }
            
            endHeight = curHeight + this.height;
            
            midHeight = Math.floor(this.height / this.times);
            Z.timer(this.speed, this.times, this, 
                function(){window.scrollTo(0, curHeight + midHeight * index++);},
                function(){window.scrollTo(0, endHeight);this.running=false;}
            );
                
            if (curHeight >= this.maxHeight - this.height)
                this.$down.fadeOut(this.speed * this.times);
                
            if (curHeight == 0)
                this.$up.fadeIn(this.speed * this.times);
        }
        else
        {//向上
            if (curHeight <= 0){
                this.running = false;
                return;
            }
        
            if (this.maxHeight == curHeight)
            {
                endHeight = Math.floor(curHeight / this.height) * this.height;
                this.$down.fadeIn(this.speed * this.times);
            }
            else
            {
                endHeight =  curHeight - this.height;
            }
                
            midHeight = (curHeight - endHeight) / this.times;
            Z.timer(this.speed, this.times, this, 
                function(){window.scrollTo(0, curHeight - midHeight * index++);},
                function(){window.scrollTo(0, endHeight);this.running=false;}
            );
                       
            if (endHeight <= 0)
                this.$up.fadeOut(this.speed * this.times);
        }
    }
};

Z.ScrollScreen.onload = function(f)
{//增加滚屏加载，当本JS加载完成之后才加载函数
    Z.onload(f);
};

//END
})(zhiqim);