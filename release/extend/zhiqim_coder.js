/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z) 
{//BEGIN 
// @version v1.1.0 @author zouzhigang 2015-11-12 新建与整理

/********************************************/
//选择框定义和加载转换成下拉列表对象
/********************************************/
Z.Coder = Z.Class.newInstance();
Z.Coder.v = "8.0.4";
Z.Coder.prototype = 
{
    defaults: 
    {
        elem: null
    },
    
    init: function()
    {//初始化
        if (!this.elem)
            return;
            
        this.$elem = Z(this.elem);
    }
};

//缓存加载函数
Z.Coder.cache = [];
Z.Coder.load = function()
{
    //1.先删除原来的缓存
    Z.each(Z.Coder.cache, function(coder){
        coder.remove();
    });
    Z.Coder.cache = [];
    
    //2.再加载所有的选择框
    var elements = Z.D.attrs("data-role", "z-coder");
    if (!elements || elements.length == 0)
        return;

    Z.each(elements, function(elem)
    {
        var $textarea = Z(elem).hidden();
        //取出父节点的绝对位置

        var offsetTop = $textarea.offsetTop();
        var offsetLeft = $textarea.offsetLeft();
        var width = $textarea.offsetWidth();
        var height = $textarea.offsetHeight();
        
        var id = Z.S.trim($textarea.attr("id"));
        var name = Z.S.trim($textarea.attr("name"))
        var classes = Z.S.trim($textarea.attr("data-class"));
        
        var coder = '<div class="z-coder">'
                  + '</div>';
        
        var $elem = Z(coder).appendToPos($textarea.parent());
        $elem.addClass(classes).css({position: "absolute", top: offsetTop, left: offsetLeft, width: width, height: height});
        $elem.attr("data-id", id).attr("data-name", name);

        Z.Coder.cache.push(new Z.Coder({elem: $elem[0]}));
    });
};

Z.onload(Z.Coder.load);

//END
})(zhiqim);