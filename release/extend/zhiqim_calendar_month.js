/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z)
{//BEGIN 

Z.Month = Z.Class.newInstance();
Z.Month.v = "8.0.4";
Z.Month.prototype = 
{
    defaults: 
    {
        top: 0,                                     //和指定的对象相差上位置
        left: 0,                                    //和指定的对象相差左位置
        
        selectYear: null,                           //选中的年,要求数字
        selectMonth: null,                          //选中的月,要求数字
        
        element: null,                              //当前对象
        elemDate: null,                             //选中的时间
        currDate: new Date()                        //保存当前时间
    },
    
    init: function()
    {
        this.id = Z.random(10);//日历ID,为保证唯一在init随机生成
        
        this.html ='<div id="Z_Month_'+this.id+'" style="position:absolute;display:none;z-index:1000;">';
        this.html+='<table class="z-table z-calendar">';
        
        //选择年月
        this.html+='<tr>';
        this.html+='    <td>';
        this.html+='    <table class="z-table z-top">';
        this.html+='    <tr>';
        this.html+='        <td width="30" align="center" class="z-pointer" id="Z_Month_prev_'+this.id+'"><div class="z-arrow z-left"></div></td>';
        this.html+='        <td width="*" align="center" class="z-year-month" id="Z_Month_year_'+this.id+'"><span class="z-default"></span></td>';
        this.html+='        <td width="30" align="center" class="z-pointer" id="Z_Month_next_'+this.id+'"><div class="z-arrow z-right"></td>';
        this.html+='    </tr>';
        this.html+='    </table>';
        this.html+='    </td>';
        this.html+='</tr>';
        
        //当前星期
        this.html+='<tr>';
        this.html+='    <td>';
        this.html+='    <table class="z-table z-day z-px14 z-pd10">';
        this.html+='    <tr>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_1">一月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_2">二月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_3">三月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_4">四月</td>';
        this.html+='    </tr>';
        this.html+='    <tr>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_5">五月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_6">六月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_7">七月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_8">八月</td>';
        this.html+='    </tr>';
        this.html+='    <tr>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_9">九月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_10">十月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_11">十一月</td>';
        this.html+='        <td class="z-tomonth" id="Z_Month_month_'+this.id+'_12">十二月</td>';
        this.html+='    </tr>';
        this.html+='    <tr>';
        this.html+='        <td colspan="2" style="color:#339a99" id="Z_Month_clear_'+this.id+'">清空</td>';
        this.html+='        <td colspan="2" style="color:#339a99" id="Z_Month_close_'+this.id+'">关闭</td>';
        this.html+='    </tr>';
        this.html+='    </table>';
        this.html+='    </td>';
        this.html+='</tr>';

        //结束
        this.html+='</table>';
        this.html+='</div>';
    },
    
    execute: function()
    {
        //解析对象中年月，如果不是年月格式置为当前月
        if (!Z.V.isDate(this.element.value+"-01"))
            this.elemDate = this.currDate;
        else
        {//有值时选中月
            this.elemDate = Z.DT.toDate(this.element.value+"-01");
            this.selectMonth = this.elemDate.getMonth()+1;
        }
        
        this.selectYear = this.elemDate.getFullYear();
        
        //组装日历DIV，和设置好位置和事件
        this.height = 271;
            
        var $element = Z(this.element);
        var top = $element.offsetTopBody() + $element.offsetHeight() + this.top;
        var left = $element.offsetLeftBody() + this.left;
        
        if (top > this.height && Z.D.clientHeight() - top - this.height < 0)
        {//如果顶部够高，底部不够高时，则向上弹出
            top -= this.height;
        }

        var $calendar = Z(this.html);
        $calendar.appendTo("body").css({top:top, left:left, display:'block'});

        //当前点击时阻止冒泡，其他点击时关闭
        $calendar.click(function(e){Z.E.stop(e);});
        Z(document).click(this.close, this);
        
        //设置向左和向右移动一年
        Z("#Z_Month_prev_"+this.id).click(this.doPrevYear, this);
        Z("#Z_Month_next_"+this.id).click(this.doNextYear, this);
        
        Z("#Z_Month_clear_"+this.id).click(function(e){this.element.value="";this.close(e);}, this);
        Z("#Z_Month_close_"+this.id).click(function(e){this.close(e);}, this);
        
        //设置选择年份月份时打开年份列表事件
        Z("#Z_Month_year_"+this.id).click(this.doOpenYearList, this);
        
        //最后显示选择年份,月份信息
        this.showYear();
        this.showMonth();
    },
    
    doOpenYearList: function(e)
    {//打开选择年份列表
        var $list = Z("#Z_Month_year_"+this.id).find("ul");
        if ($list.length > 0)
            $list.remove();
        else
        {
            var min = this.selectYear - 50;
            var max = this.selectYear + 50;
            var $ul = Z("<ul></ul>").addClass("z-year-list");
            for (var i=min;i<=max;i++)
            {
                var $option = Z("<span value='" + i + "'>" + i + "年" + "</span>");
                $option.click(this.onChangeYear, this);
                if (i == this.selectYear)
                    $option.addClass("z-selected");
                $ul.append($option);
            }
            $ul.append("<span class='z-close'>关闭</span>");
            Z("#Z_Month_year_"+this.id).append($ul);
            //把滚动条移到中间位置
            $ul[0].scrollTop = $ul[0].scrollHeight/2 - 117;
        }
    },
    
    onChangeYear: function(e)
    {//修改年份
        var value = Z(Z.E.target(e)).val();
        this.selectYear = parseInt(value);
        
        Z("#Z_Month_year_"+this.id).find("ul").hide().remove();
        
        this.showYear();
        this.showMonth();
        
        Z.E.stop(e);
    },
    
    doPrevYear: function()
    {//上一年
        this.selectYear -= 1;
        this.showYear();
        this.showMonth();
    },
    
    doNextYear: function()
    {//下一年
        this.selectYear += 1;
        this.showYear();
        this.showMonth();
    },
    
    close: function(e)
    {//关闭
        var target = Z.E.target(e);
        if (target === this.element)
        {//如果是指定的元素不关闭
            Z.E.cancel(e);
            return;
        }
           
        Z(document).offclick(this.close, this);
        Z("#Z_Month_"+this.id).remove();
    },
    
    showYear: function() 
    {//显示年
        Z("#Z_Month_year_"+this.id).find(".z-default").html(this.selectYear+"年");
    },
    
    showMonth: function()
    {//显示月
        //先统一背景等数据
        for (var i=1;i<=12;i++)
        {
            Z("#Z_Month_month_"+this.id+"_"+i).removeClass("z-today").removeClass("z-selected").click(function(e)
            {
                var id = Z.E.current(e).id;
                var ind = id.lastIndexOf("_");
                this.selectMonth = parseInt(id.substring(ind+1));
                this.setSelectValue();
                this.close(e);
                
            }, this);
        }
        
        if (this.selectYear == this.currDate.getFullYear())
            Z("#Z_Month_month_"+this.id+"_"+(this.currDate.getMonth()+1)).addClass("z-today");
        
        if (this.selectMonth != null)
            Z("#Z_Month_month_"+this.id+"_"+this.selectMonth).addClass("z-selected");
    },
    
    setSelectValue: function()
    {
        this.setValue(this.selectYear + "-" + Z.S.prefixZero(this.selectMonth, 2));
    },
    
    setValue: function(newValue)
    {
        var oldValue = this.element.value;
        this.element.value = newValue;
        if (this.element.onchange && oldValue != this.element.value)
            this.element.onchange();
    }
};

Z.month = function($elem)
{
    return new Z.Month({immediate:true, element:$elem});
}

//END
})(zhiqim);