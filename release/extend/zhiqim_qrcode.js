/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。
 * 
 * 指定登记&发行网站： https://www.zhiqim.com/ 欢迎加盟知启蒙，[编程有你，知启蒙一路随行]。
 *
 * 本文采用《知启蒙登记发行许可证》，除非符合许可证，否则不可使用该文件！
 * 1、您可以免费使用、修改、合并、出版发行和分发，再授权软件、软件副本及衍生软件；
 * 2、您用于商业用途时，必须在原作者指定的登记网站，按原作者要求进行登记；
 * 3、您在使用、修改、合并、出版发行和分发时，必须包含版权声明、许可声明，及保留原作者的著作权、商标和专利等知识产权；
 * 4、您在互联网、移动互联网等大众网络下发行和分发再授权软件、软件副本及衍生软件时，必须在原作者指定的发行网站进行发行和分发；
 * 5、您可以在以下链接获取一个完整的许可证副本。
 * 
 * 许可证链接：http://zhiqim.org/licenses/zhiqim_register_publish_license.htm
 *
 * 除非法律需要或书面同意，软件由原始码方式提供，无任何明示或暗示的保证和条件。详见完整许可证的权限和限制。
 */
 
+(function(Z)
{

/****************************************/
//“二维码生成”
/****************************************/
/** 实现方法一 **/
Z.Qrcode = Z.Class.newInstance();
Z.Qrcode.prototype = 
{
    defaults: 
    {//定义
        elem: null,                           // 二维码展示的对象节点
        text: "",                             // 需转换成二维码的字符串
        render: "canvas",                     // 实现方式，可选："canvas"、"table"
        width: 256,                           // 二维码的宽度
        height: 256,                          // 二维码的高度
        typeNumber: -1,                       // < 1，则自动计算，具体作用暂时不明
        correctLevel: 2,                      // 取值范围[1-4]，具体作用暂时不明
        background: "#ffffff",                // 背景色
        foreground: "#000000"                 // 二维码颜色
    },

    execute: function()
    {//执行
    
        this.$elem = Z.$elem(this.elem, "Z.Qrcode");
        if (Z.V.isEmptyBlank(this.text))
        {//转换的字符必须
            return Z.alert("请输入需要转换的字符串！");
        }
        
        //创建qrcode对象
        var qrcode  = new QRCode(this.typeNumber, this.correctLevel);
        qrcode.addData(this.text);
        qrcode.make();
        
        //生成二维码
        this.$elem.append(this.render == "canvas" ? this.createCanvas(qrcode) : this.createTable(qrcode));
    },
    
    createCanvas: function(qrcode)
    {//默认画布方式
    
        //创建画布
        var canvas  = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        var ctx = canvas.getContext('2d');

        //计算网格模块宽高
        var tileW   = this.width  / qrcode.getModuleCount();
        var tileH   = this.height / qrcode.getModuleCount();

        //生成画布
        for( var row = 0; row < qrcode.getModuleCount(); row++ )
        {
            for( var col = 0; col < qrcode.getModuleCount(); col++ )
            {
                ctx.fillStyle = qrcode.isDark(row, col) ? this.foreground : this.background;
                var w = (Math.ceil((col+1)*tileW) - Math.floor(col*tileW));
                var h = (Math.ceil((row+1)*tileW) - Math.floor(row*tileW));
                ctx.fillRect(Math.round(col*tileW),Math.round(row*tileH), w, h);  
            }   
        }
        
        return canvas;
    },
    
    createTable: function(qrcode)
    {//表格方式
        
        //创建表格
        var $table  = Z('<table></table>')
            .css({"width": this.width, "height": this.height})
            .css("border", "0px")
            .css("border-collapse", "collapse")
            .css('background-color', this.background);
      
        //计算网格模块宽高
        var tileW = this.width / qrcode.getModuleCount();
        var tileH = this.height / qrcode.getModuleCount();

        //生成表格数据
        for(var row = 0; row < qrcode.getModuleCount(); row++ )
        {
            var $row = Z('<tr></tr>').css('height', tileH).appendTo($table);
            for(var col = 0; col < qrcode.getModuleCount(); col++ )
            {
                Z('<td></td>')
                    .css('width', tileW)
                    .css('background-color', qrcode.isDark(row, col) ? this.foreground : this.background)
                    .appendTo($row);
            }   
        }

        return $table;
    },
}

/** 实现方法二 **/
Z.Q.fn.qrcode = function(options)
{
    if( typeof options === 'string' ){
        options = {text: options};
    }
    
    var qrcode = new Z.Qrcode();
    qrcode.elem = this;
    for (var i in options){
        qrcode[i] = options[i];
    }
    qrcode.execute();
}

//END
})(zhiqim);