/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z) 
{//BEGIN 
// @version v1.1.0 @author zouzhigang 2015-11-12 新建与整理

Z.Fullscreen =
{//moz是FullScreen，其他都是Fullscreen
    target: null,
    elem: function()
    {//全屏对象
        return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement
             || document.msFullscreenElement || document.oFullscreenElement || Z.Fullscreen.target;
    },
    enabled: function()
    {//是否开启全屏
        return document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled
             || document.msFullscreenEnabled || document.oFullscreenEnabled;
    },
    change: function(func)
    {//更改方法
        var name = null;
        if (document.body.requestFullscreen) 
            name = "fullscreenchange";
        else if(document.body.webkitRequestFullscreen)
            name = "webkitfullscreenchange";
        else if(document.body.mozRequestFullScreen) 
            name = "mozfullscreenchange";
        else if(document.body.msRequestFullscreen)
            name = "msfullscreenchange";
        else if(document.body.oRequestFullscreen)
            name = "ofullscreenchange";

        if (name != null)
        {//支持则增加监听
            Z.E.add(document, name, func);
        }
    },
    full: function(id)
    {
        var elem = Z.D.id(id);
        if (elem.requestFullscreen) 
            elem.requestFullscreen();
        else if(elem.webkitRequestFullscreen)
            elem.webkitRequestFullScreen();
        else if(elem.mozRequestFullScreen) 
            elem.mozRequestFullScreen();
        else if(elem.msRequestFullscreen)
            elem.msRequestFullscreen();
        else if(elem.oRequestFullscreen)
            elem.oRequestFullscreen();
            
        Z.Fullscreen.target = elem;
    },
    toggle: function()
    {
        Z.Fullscreen.change(Z.Fullscreen.toggleHandler);
    },
    toggleHandler: function()
    {
        var elem = Z.Fullscreen.elem();
        if (elem.paused)
            elem.play();
        else
            elem.pause();
    }
};

Z.onload(function()
{//全屏播放，退出暂停
    Z.Fullscreen.toggle();
});

//END
})(zhiqim);