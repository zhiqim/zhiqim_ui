/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z)
{
//BEGIN

Z.ImagePaste = Z.Class.newInstance();
Z.ImagePaste.prototype = 
{
    defaults:
    {//传入数据存放元素和显示元素对象或编号
        dataElem: null,
        showElem: null,
        callback: null
    },
    
    execute: function()
    {
        this.$dataElem = Z.$elem(this.dataElem, "Z.ImagePaste");
        this.$showElem = Z.$elem(this.showElem, "Z.ImagePaste");
        
        //注册粘贴事件
        Z(window).on("paste", this.onPaste, this);
    },
    
    onPaste: function(e)
    {//粘贴
        var data = e.clipboardData;
        if(!data || !data.items || !data.types)
        {//无数据结束
            return;
        }
        
        //找到一个文件类型，并且是图片格式的
        var theItem;
        for(var i=0;i<data.types.length;i++)
        {
            if(data.types[i] !== "Files")
                continue;
            
            var item = data.items[i];
            if (!item || item.kind != "file" || !(/^image\//i.test(item.type)))
                continue;
            
            theItem = item;
            break;
        }

        if (!theItem)
        {//未找到结束
            return;
        }
        
        var reader = new FileReader();
        reader.onload = Z.bind(this.onRead, this);
        reader.readAsDataURL(theItem.getAsFile());
    },
    
    onRead: function(e)
    {
        var result = e.target.result;
        if (Z.EL.has(this.$dataElem[0], "value"))
            this.$dataElem.val(result);
        else
            this.$dataElem.text(result);
        
        this.$showElem.html("<img src="+result+">");
        
        if (Z.T.isFunction(this.callback))
        {//回调
            this.callback.call(this);
        }
    }
}

//END
})(zhiqim);