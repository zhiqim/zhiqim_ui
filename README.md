### 什么是“知启蒙前端框架”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;知启蒙前端框架（ZhiqimUI）是一套集成Javascript库、Css库、Font库、常用ico图标等，并在其上开发的大量UI控件组成的前端开发套件，是Zhiqim Development Kit的前端核心。

<br>

### “知启蒙前端框架”有什么优点？
---------------------------------------
1、ZhiqimUI的JS库拥有类似于jQuery的DOM操作能力，通过Z(selector)方式访问，并集成了大量常用的静态工具。<br>
2、ZhiqimUI的Css库类似于bootstrap，统一使用z-前缀约束，定义了大量常用的css规则，同时兼容其他的css库。<br>
3、ZhiqimUI的插件库非常丰富并持续增加中，有对话框、Ajax调用、表单套件、日历、复制、标签页、文件上传等。<br>
4、ZhiqimUI压缩打包成一个jar文件（zhiqim_ui.jar），配合ZhiqimHttpd使用，可直接通过/service/res/*访问。<br>

<br>


### 知启蒙前端框架目录结构
---------------------------------------
![知启蒙前端框架目录结构](https://zhiqim.org/project/images/164626_37d2147a_2103954.png "知启蒙前端框架目录结构")
<br>


### 如何使用知启蒙前端框架？&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.zhiqim.com/document/tutorial/ui/test.htm" target="_blank">打开演示地址</a>
---------------------------------------

```
<!DOCTYPE html>
<html>
<head>
<title>知启蒙前端框架</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
<meta name="Author" content="知启蒙" />
<meta name="Keywords" content="ZhiqimUI 知启蒙 zhiqim 开发教程 Java HTML5"/>
<meta name="Description" content="知启蒙前端框架（ZhiqimUI）是一套集成Javascript库、Css库、Font库、常用ico图标等，并在其上开发的大量UI组件组成的前端开发套件。"/>

<!-- 引入zhiqimUI的css文件 -->
<link rel="stylesheet" href="https://zhiqim-org.oss-cn-shenzhen.aliyuncs.com/service/res/zhiqim_v1.5.0.r2018091201.css">

<!-- 引入zhiqimUI的js文件 -->
<script src="https://zhiqim-org.oss-cn-shenzhen.aliyuncs.com/service/res/zhiqim_v1.5.0.r2018091201.min.js"></script>

<script>
var i = 1;
function doUpdate()
{
    //以下代码将把id为text的input的value值改为"新的内容"。
    Z("#text").val("新的内容"+(i++));
}

function doDialog()
{
    //以下代码弹出一个对话框。支持两种（dialog.text和dialog.url）
    var dialog = new Z.Dialog();
    dialog.text = "<div class='z-absolute-center-middle z-w100 z-h60 z-px20'>对话框内容</div>";//弹出对话框的内置HTML
    //dialog.url = location.href;//使用iframe弹出加新URL
    dialog.width = 500;
    dialog.height = 300;
    dialog.execute();
}
</script>
</head>

<body>
<!-- 使用zhiqimUI的CSS定义 -->
<table class="z-table z-bordered z-bd-rd8 z-absolute-center-middle zi-w300 z-h200">
<tr>
    <td class="z-text-center">
        <input id="text" class="z-input z-float-left" value="原内容">&nbsp;&nbsp;<button class="z-button z-blue z-float-left" onclick="doUpdate();">点击</button>
    </td>
</tr>
<tr>
    <td class="z-text-center">
        <button class="z-button z-blue z-large" onclick="doDialog();">弹出对话框</button>
    </td>
</tr>
</table>
</body>
</html>
```

<br>


### 知启蒙前端框架CSS核心库
---------------------------------------
<table style="border-collapse:collapse; border:1px solid #333; height:400px; width:100%">
<tr bgcolor="#f5f5f5">
    <td width="30%">功能</td>
    <td width="*">说明</td>
</tr>
<tr>
    <td><a href="https://www.zhiqim.com/document/tutorial/ui/css/css.htm" target="_blank">全局定义</a></td>
    <td>全局定义是指在全局定义里的标签缺省下的样式规则</td>
</tr>
<tr>
    <td>颜色定义</td>
    <td>颜色定义是指在ZhiqimUI的核心CSS文件中，默认提供的全局、白色、黑色和红、橙、黄、绿、青、蓝、紫共十种颜色的定义，包括字体颜色、背景颜色和边框颜色等，以及对应的样式名称。</td>
</tr>
<tr>
    <td>字体图标</td>
    <td>“字体图标”是矢量图片按字体规范设计的一种新的图标，知启蒙字体图标放在/service/res/font/zhiqim.woff文件中，当前开发和收集的图标分
        操作类、文档类、设备类、图表类、实体类五种，共五十九个图标。
        字体图标不支持斜体，以便使用&lt;i&gt;标签来定义，如&lt;i class="z-font z-config"&gt;&lt;/i&gt;
    </td>
</tr>
<tr>
    <td>图片图标</td>
    <td>“图片图标”是把一组小图标图片拼成一张大图，通过CSS的background-position特性指定小图标在大图中的位置和宽高显示图标，
        知启蒙图片图标放在/service/res/image目录下，根据大小和类型分为12px、16px、22px、36px以及文件图标、树图标等，在这里还利用三角型和圆型生成箭头和圆圈等样式。
    </td>
</tr>
<tr>
    <td>文本样式</td>
    <td>文本样式是指对文本文字增加样式规则，如指定文本粗斜体、文本上下标、文本字体大小、文本行高、文本强制换行、强制不换行、省略隐藏、文本标题属性、文本对齐方式和文本附带的下划线删除线、以及编码突出效果等。</td>
</tr>
<tr>
    <td>块样式</td>
    <td>块样式是指对一个块区域增加样式规则，如指定块隐藏显示、块定位、块宽、块宽百分比、块高、块高百分比、内边距、外边距等属性。</td>
</tr>
<tr>
    <td>表格样式</td>
    <td>表格样式是指对表格增加样式规则，如指定表格边框、表格圆角、表格行线、表格单元格内边距、表格单元格字体大小、表格单元格文本对齐方式、表格单元格文本行高等，默认表格无边框。
        另外，CSS对底部圆角当有背景时需要手动对第一和最后单元格补圆角，当圆角首列合并单元格时也需要对被合并的行第一单元格补左边线。
     </td>
</tr>
</table>

<br>


### 知启蒙前端框架JS核心库
---------------------------------------
<table style="border-collapse:collapse; border:1px solid #333; height:400px; width:100%">
<tr bgcolor="#f5f5f5">
    <td width="30%">功能</td>
    <td width="*">说明</td>
</tr>
<tr>
    <td>全局定义</td>
    <td>知启蒙JS统一命名空间为“Z”，同时“Z”也是window下全局函数，如果“Z”有定义冲突请使用全称“zhiqim”。在全局“Z”命名空间下定义的全局属性、全局函数、全局正则表达式和全局类。</td>
</tr>
<tr>
    <td>静态对象</td>
    <td>静态对象是把一系列常用的对象定义成静态属性和函数。目前包括有浏览器对象（Z.B）、文档对象（Z.D）、
        事件对象（Z.E）、元素对象（Z.EL）、HTML对象（Z.H）、地址对象（Z.L）、COOKIE对象（Z.CK）。
    </td>
</tr>
<tr>
    <td>工具对象</td>
    <td>工具对象也是静态对象的一种，把一系列常用的静态属性和函数归类到一个工具对象中。目前包括断言工具（Z.A/Z.Asserts）、对象工具（Z.O/Z.Objects）、类型工具（Z.T/Z.Types）、
        验证工具（Z.V/Z.Validates）、表单工具（Z.F/Z.FM）、JSON工具（Z.J/Z.Jsons）、日期工具（Z.DT/Z.DateTimes）、数组工具（Z.AR/Z.Arrays）、字符串工具（Z.S/Z.Strings）、算术工具（Z.Maths）、
        金额工具（Z.AM/Z.Amounts）和颜色工具（Z.Colors）。
    </td>
</tr>
<tr>
    <td>查询对象Z.Query</td>
    <td>Z.Query是设计成类似jQuery的DOM操作类。Z.Query采用HTML5原生的document.querySelectorAll实现了大部分jQuery的API，如selector，DOM操作，事件方法，动画等。
        并增加了自身的一些特性和命名，如offsetLeftBody()、focusEnd()等。
    </td>
</tr>
</table>

<br>


### 知启蒙前端框架插件库
---------------------------------------
<table style="border-collapse:collapse; border:1px solid #333; height:400px; width:100%">
<tr bgcolor="#f5f5f5">
    <td width="30%">功能</td>
    <td width="*">说明</td>
</tr>
<tr class="z-bg-cyan">
    <td colspan="2">表单插件库</td>
</tr>
<tr>
    <td>输入框</td>
    <td>输入框样式是指对输入框增加样式规则，如指定输入框长度大小、边框颜色、和输入控制等。</td>
</tr>
<tr>
    <td>按钮</td>
    <td>按钮样式是指对按钮增加样式规则，按钮分为三种，一是标准按钮、二为筛选按钮、三是多选按钮。样式规则包括按钮大小、颜色、边框颜色、选中特效等。</td>
</tr>
<tr>
    <td>多选框</td>
    <td>多选框样式是指参考多选按钮设计出有颜色设定的多选的方框样式。多选框默认1像素边框，移入时变为2像素边框，选中后增加背景颜色并显示打勾图标表示已选中，多选框默认黑色，支持红橙黄绿青蓝紫等颜色和大小5种设定。</td>
</tr>
<tr>
    <td>单选框</td>
    <td>单选框样式是指参考单选按钮设计出有颜色设定单选的圆框的样式。单选框默认1像素边框，移入时变为2像素边框，选中后增加背景颜色并显示打勾图标表示已选中，单选框默认黑色，支持红橙黄绿青蓝紫等颜色和大小5种设定。</td>
</tr>
<tr>
    <td>选择框</td>
    <td>选择框样式是参考选择按钮设计出有颜色设定下拉列表的样式。选择框是一种下拉列表样式，目标是和表单选择按钮联动，实现跨浏览器且更美观的样式。</td>
</tr>
<tr>
    <td>可编辑文本</td>
    <td>可编辑文本是定义一段文本（span），在该文本之后增加一个笔状图标，当点击笔状图标时显示输入框和保存按钮，输入框支持输入控制，点击保存按钮触发保存事件。</td>
</tr>
<tr>
    <td>日历</td>
    <td>日历控件是在输入框触发时展开一个年月日时分秒的日期界面，按日历格式显示出来，通过点击和选择得到要求的日期或时间。当前日历控件支持选择日期、选择日期+时间、选择日期+时分三种。</td>
</tr>
<tr class="z-bg-cyan">
    <td colspan="2">对话框插件库</td>
</tr>
<tr>
    <td>自定义对话框</td>
    <td>自定义对话框是指根据业务要求，自动组装对话框参数，完成对话框的弹出。自定义对话框支持的参数有标题、宽度、高度、边框颜色、目标区域、指定弹出HTML、指定弹出URL等一系列参数，是各种对话框的基础。</td>
</tr>
<tr>
    <td>告警对话框</td>
    <td>警告对话框是自定义对话框的一种参考alert方法生成的对话框。目标是在屏幕的中间提供没有警告声音且美观的警告对话框，支持设置警告内容和点击确定后回调函数。</td>
</tr>
<tr>
    <td>询问对话框</td>
    <td>询问对话框是自定义对话框的一种参考confirm方法生成的对话框。目标是在屏幕的中间提供没有警告声音且美观的确认对话框，支持设置确认内容和点击确定后回调函数，点击取消不向下处理。</td>
</tr>
<tr>
    <td>提示对话框</td>
    <td>提示对话框是自定义对话框的一种参考prompt方法生成的对话框。目标是在屏幕的中间提供没有警告声音且美观的提示对话框，支持设置提示内容和点击确定后回调函数，点击取消不向下处理。</td>
</tr>
<tr>
    <td>加载对话框</td>
    <td>加载对话框是自定义对话框的一种没有标题只有加载中提示的对话框。目标是在屏幕或用户指定的元素可视范围的中间提供没有警告声音且美观的加载对话框，支持设置九个参数来定义加载中界面，无关闭按钮，由业务实现关闭。</td>
</tr>
<tr>
    <td colspan="2">导航插件库</td>
</tr>
<tr>
    <td>标签页</td>
    <td>标签页是参考tabnav设计出更简单易用的内容重叠布局的导航页。标签页使用nav和section作为是标签和内容显示。在nav中使用li表示每个标签，在section中使用div作为每个标签显示的内容。</td>
</tr>
<tr>
    <td>步骤条</td>
    <td>步骤条是通过指定多个步骤标签，在每步中展示激活的一个步骤标签的导航页。步骤条使用nav作为每个步骤。
        样式通过before和after属性自动在每个步骤后加上白色箭头。
    </td>
</tr>
<tr>
    <td>提示框</td>
    <td>提示框是当鼠标移入或点击到重要的标志时，主动冒泡出一个浮动层，对该标志作解释的提示信息框。当前支持鼠标移入和点击两种事件，提示框在设计中有箭头、对齐方式、边框等属性。</td>
</tr>
<tr>
    <td>下拉列表</td>
    <td>下拉列表是设计出当移入或点击时下拉出一个列表的样式。下拉列表当前支持点击和移入触发下拉列表一层界面，支持列表颜色、激活颜色，指定下拉列表宽度和多列等样式。</td>
</tr>
<tr>
    <td colspan="2">工具插件库</td>
</tr>
<tr>
    <td>Flash复制</td>
    <td>由于各浏览器剪贴板功能不一致，功能使用复杂，因此知启蒙是利用Flash的sprite容器制作的剪贴板功能，以达到跨浏览器支持对文本的复制功能。
        设计方案是先用button占位，再利用flash透明的特性覆盖button。该功能需要开启flash的支持。</td>
</tr>
<tr>
    <td>Ajax调用</td>
    <td>AJAX即“Asynchronous Javascript And XML”（异步JavaScript和XML），是指一种创建交互式网页应用的网页开发技术。
        知启蒙AJAX采用面向RMI的概念设计，通过指定类和方法，传入参数对应方法的参数，通过RMI访问JAVA类的方法获取执行结果。
    </td>
</tr>
<tr>
    <td>拖拽</td>
    <td>拖拽是指定一个可拖动的对象，当鼠标按下时拖拽到其他位置。缩放是指定一个可缩放的对象，当鼠标按下时拉动对象使之放大或缩小。</td>
</tr>
<tr>
    <td>文件上传</td>
    <td>知启蒙文件上传提供了多种上传方法，支持表单上传、单文件上传，多文件上传，预览再上传，大文件上传断点续传（HTML5和Flash两种）等。</td>
</tr>
</table>

<br>

### 知启蒙技术框架与交流
---------------------------------------

![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多知启蒙标识语言，[【请戳这里】](https://zhiqim.org/project/zhiqim_framework/zhiqim_ui/tutorial/index.htm)
