/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z) 
{//BEGIN 
// @version v1.1.0 @author zouzhigang 2015-11-12 新建与整理

/********************************************/
//标签页定义、加载和转换
/********************************************/
Z.Tabnav = {};
Z.Tabnav.onclick = function(e)
{
    //标签页切换选中
    var li = Z.E.current(e);
    Z(li).addClass("z-active").siblings("li").removeClass("z-active");
    
    //找到标签页是第n个
    var nth = Z(this).nth(li, ">nav>ul>li");

    //内容页切换显示
    Z(this).find(">section>div").removeClass("z-active");
    Z(this).find(">section>div:nth-child("+nth+")").addClass("z-active");
    
    //调用事件
    if (this.onchange){
        this.onchange();
    }
    
    Z.E.forbidden(e);
}

//缓存加载函数
Z.Tabnav.cache = [];
Z.Tabnav.load = function()
{
    //1.先删除原来的缓存的点击事件
    Z.each(Z.Tabnav.cache, function(elem){
        Z(elem).offclick(Z.Tabnav.click, elem);
    });
    Z.Tabnav.cache = [];
    
    //2.再加载所有的标签页
    Z("[data-role=z-tabnav]").each(function(elem)
    {
        Z(elem).find(">nav>ul>li").each(function(li)
        {
            Z(li).click(Z.Tabnav.onclick, elem);
            Z.Tabnav.cache.push(elem);
        });
    });
};

Z.onload(Z.Tabnav.load);

//END
})(zhiqim);