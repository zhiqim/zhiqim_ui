/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

+(function(Z)
{
/****************************************/
//“操作”弹窗
/****************************************/
Z.Popup = Z.Class.newInstance();

Z.Popup.cache = new Z.HashMap();
Z.Popup.close = function(id, immediate)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    if (!id)
    {//删除全部
        Z.each(Z.Popup.cache.values(), function(zmPopup){
            zmPopup.fadeOut(immediate);
        });
        Z.Popup.cache.clear();
    }
    else
    {//找到id删除
        var zmPopup = Z.Popup.cache.get(id);
        if (zmPopup)
        {
            zmPopup.fadeOut(immediate);
            Z.Popup.cache.remove(id);
        }
    }
};

Z.Popup.get = function(id)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    return Z.Popup.cache.get(id);
};

Z.Popup.prototype =
{
    //start of Z.Popup.prototype
    defaults:
    {//定义
        //基础参数
        target: null,          //指定弹出对象
        start: "left",         //弹窗起始位置：left/top/right/bottom
        overlay: "all",        //覆盖部分：all/part
        overlayRatio: "75",    //覆盖该主轴上的比例
    },

    validate: function()
    {//验证参数是否有误
        if (!this.target)
        {//null，或为空、0
            Z.alert("Z.Popup参数“target”未设置！");
            return false;
        }
        if (Z.T.isString(this.target) || Z.T.isObject(this.target))
        {//字符串类型，或者对象类型
            if (!Z(this.target)[0]) {
                Z.alert("Z.Popup参数“target”设置不正确！");
                return false;
            }
        }
        if (!Z.AR.contains(["left","top","right","bottom"],this.start)){
            this.start = "left";
        }
        if (!Z.AR.contains(["part","all"],this.overlay)){
            this.start = "part";
        }
        return true;
    },

    execute: function()
    {//执行
        if (!this.validate()){
            return;
        }
        this.id = Z(this.target).attr("id");
        if (!this.id){
            this.id = "Z_zmPopup_" + Z.Ids.uuid();
            Z(this.target).attr(this.id);
        }
        Z.Popup.cache.put(this.id,this);
        //定义popup
        this.creatPopup();
        //事件绑定
        this.attachEvent();
        //执行弹出
        this.fadeIn();
    },

    creatPopup: function()
    {//创建popup盒子
        this.$popup = Z(this.target);
        this.$popup.removeClass("z-overlay-all").removeClass("z-overlay-part")
            .addClass("z-overlay-" + this.overlay);
        this.$popup.removeClass("z-top").removeClass("z-right").removeClass("z-bottom").removeClass("z-left")
            .addClass("z-" + this.start).css("display","none");

        this.$bg = this.$popup.find(".z-popup-bg");
        this.$main = this.$popup.find(".z-popup-main");
        this.$title = this.$popup.find(".z-popup-title");
        this.$sure = this.$popup.find(".z-popup-sure");
        this.$cancel = this.$popup.find(".z-popup-cancel");
        this.$con = this.$popup.find(".z-popup-content");
    },

    attachEvent: function()
    {//事件绑定
        this.$bg.on("touchstart",this.fadeOut,this);
        this.$cancel.on("touchstart",this.fadeOut,this);
        if (this.$sure[0]){
            this.$sure[0].$this = this;
            this.$sure.on("touchstart",function(ev){
                this.$this.fadeOut(ev);
            })
        };
        var transitionEndEvent = this.transitionFixed();
        this.$main.on(transitionEndEvent,this.transitionEnd,this);
    },

    fadeIn: function()
    {//显示弹窗
        var that = this;
        var req = window.requestAnimationFrame
            || window.webkitRequestAnimationFrame
            || window.mozRequestAnimationFrame
            || window.oRequestAnimationFrame
            || function(fun){window.setTimeout(fun,1000/60)};
        req(function(){
            that.$popup.css("display","block");
            req(function(){
                that.$popup.addClass("z-popup-fadeIn");
                that = null;
            });
        });
    },

    fadeOut: function(ev)
    {//隐藏弹窗
        Z.E.forbidden(ev);
        this.$popup.removeClass("z-popup-fadeIn");
        this.$popup.addClass("z-popup-fadeOut");
        Z.Popup.cache.remove(this.id);
        if (Z(this.target).attr("id").indexOf("Z_zmPopup_") > -1){
            Z(this.target).removeAttr("id");
        }
    },

    transitionEnd: function(ev)
    {//动画结束，退出动画结束时触发
        if(this.$popup.hasClass("z-popup-fadeOut")){
            this.$popup.css("display","none");
            this.$popup.removeClass("z-popup-fadeOut");
        }
    },

    transitionFixed: function()
    {//兼容写法
        var $div = document.createElement('div');
        if ($div.style["transition"] !== undefined ){
            $div = null;
            return "transitionend";
        }
        if ($div.style["OTransition"] !== undefined ){
            $div = null;
            return "oTransitionEnd";
        }
        if ($div.style["WebkitTransition"] !== undefined ){
            $div = null;
            return "webkitTransitionEnd";
        }
    },
    //end of Z.Popup.prototype
}

/****************************************/
//“操作列表”，底部展示/选择按钮
/****************************************/
Z.ActionList = Z.Class.newInstance();

Z.ActionList.cache = new Z.HashMap();
Z.ActionList.close = function(id, immediate)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    if (!id)
    {//删除全部
        Z.each(Z.ActionList.cache.values(), function(actionList){
            actionList.fadeOut(immediate);
        });
        Z.ActionList.cache.clear();
    }
    else
    {//找到id删除
        var actionList = Z.ActionList.cache.get(id);
        if (actionList)
        {
            actionList.fadeOut(immediate);
            Z.ActionList.cache.remove(id);
        }
    }
};

Z.ActionList.get = function(id)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    return Z.ActionList.cache.get(id);
};

Z.ActionList.prototype =
{
    //start of Z.ActionList.prototype
    defaults:
    {//定义
        id: "",          //按钮ID，可选
        html: "",        //内容自定义
        height: .7,      //高度限制
    },

    validate: function()
    {//验证参数是否有误
        if (Z.V.isEmpty(this.html)){
            Z.alert('Z.ActionList参数“html”未设置！');
            return false;
        }
        if (!this.id) {
            this.id = "Z_actionList_" + Z.Ids.uuid();
        }
        return true;
    },

    execute: function()
    {//执行
        if (!this.validate){
            return;
        }
        Z.ActionList.cache.put(this.id,this);
        //定义popup
        this.creatActionList();
        //事件绑定
        this.attachEvent();
        //执行弹出
        this.fadeIn();
    },

    creatActionList: function()
    {//创建popup盒子
        // language=HTML
        var listHtml = '<div class="z-popup z-overlay-part z-bottom"><div class="z-popup-bg"></div><div class="z-popup-main"><div class="z-popup-content"></div></div></div></div>';
        this.$action = Z(listHtml);
        this.$action.appendTo(Z("body"));

        //touchend 事件
        Z.ResetTouchEnd.load(this.$action);

        //数值按钮
        var $content = this.$action.find(".z-popup-content");
        $content.htmlc(this.html);
        if (this.html.match(/data-role\s*=\s*"z-numInput"/)){
            Z.NumInput.load($content);
        }

        this.$bg = this.$action.find(".z-popup-bg");
        this.$main = this.$action.find(".z-popup-main");
        this.$con = this.$action.find(".z-popup-content");

        //计算高度
        var docHeight = parseInt(Z.D.clientHeight());
        var conHeight = docHeight * this.height;
        this.$con.css("height",conHeight)
    },

    attachEvent: function()
    {//事件绑定
        this.$bg.on("touchstart",this.fadeOut,this);
        var transitionEndEvent = this.transitionFixed();
        this.$main.on(transitionEndEvent,this.transitionEnd,this);
    },

    fadeIn: function()
    {//显示弹窗
        var that = this;
        var req = window.requestAnimationFrame
            || window.webkitRequestAnimationFrame
            || window.mozRequestAnimationFrame
            || window.oRequestAnimationFrame
            || function(fun){window.setTimeout(fun,1000/60)};
        req(function(){
            that.$action[0].style.display = "block";
            req(function(){
                that.$action.addClass("z-popup-fadeIn");
            });
        });
    },

    fadeOut: function()
    {//隐藏弹窗
        Z.ActionList.cache.remove(this.id);
        this.$action.removeClass("z-popup-fadeIn");
        this.$action.addClass("z-popup-fadeOut");
    },

    transitionEnd: function(ev)
    {//动画结束，退出动画结束时触发
        if(this.$action.hasClass("z-popup-fadeOut")){
            this.$action.remove();
        }
    },

    transitionFixed: function()
    {//兼容写法
        var $div = document.createElement('div');
        if ($div.style["transition"] !== undefined ){
            $div = null;
            return "transitionend";
        }
        if ($div.style["OTransition"] !== undefined ){
            $div = null;
            return "oTransitionEnd";
        }
        if ($div.style["WebkitTransition"] !== undefined ){
            $div = null;
            return "webkitTransitionEnd";
        }
    },
    //end of Z.ActionList.prototype
}


//END
})(zhiqim);
