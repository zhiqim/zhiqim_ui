/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z) {

/****************************************/
// mobile 端 Z.Dialog
/****************************************/

Z.Dialog = Z.Class.newInstance();
Z.Dialog.v = "8.0.4";
/****************************************/
//定义Z.Dialog下的静态属性和关闭所有的方法
/****************************************/
Z.Dialog.cache = new Z.HashMap();
Z.Dialog.close = function(id, immediate)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    if (!id)
    {//删除全部
        Z.each(Z.Dialog.cache.values(), function(dialog){dialog.remove(immediate);});
        Z.Dialog.cache.clear();
    }
    else
    {//找到id删除
        var dialog = Z.Dialog.cache.get(id);
        if (dialog)
        {
            dialog.remove(immediate);
            Z.Dialog.cache.remove(id);
        }
    }
};
Z.Dialog.get = function(id)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    return Z.Dialog.cache.get(id);
};

/****************************************/
//定义Z.Dialog下的原型属性和方法
/****************************************/
Z.Dialog.prototype =
{
    defaults:
    {
        //基础参数9个参数
        id: null,
        title:"提示",
        borderColor:"#ccc",
        timeout: 0,
        callback: null,

        //内容相关2个参数
        text: null,
        url: null,

        //标题控制3个参数
        hasTitle:true,
        titleBgColor: "#ffffff",

        //以下3个参数为[遮罩层]参数
        shadow:true,
        shadowColor:"#555555",
        shadowOpacity: 0.6,

        //自定义内部参数
        status: 0
    },

    init: function()
    {
        if (!this.id)
        {//没传入id则随机新建
            this.id = Z.random(10);
        }
    },

    validate: function()
    {
        if (Z.D.has("Z_Dialog_"+this.id))
        {
            alert("[Z.Dialog]您传入了相同的窗口ID,如无必须可以不传入由系统随机生成");
            return false;
        }

        if (!this.text)
        {
            alert("[Z.Dialog]您需传入的text参数值");
            return false;
        }
        return true;
    },

    execute: function()
    {
        //1.检查参数
        if (!this.validate())
            return;

        //2.打开对话框和加载内容，放置到body和cache中
        var dialog = '<div class="z-w270 zi-absolute-center-middle z-flexBox z-justify-center z-flex-center zi-bg-none" id="Zm_Dialog_'+this.id+'" style="z-index:9999;"></div>';
        var title = '<h4 class="z-w100p z-pd-l30 z-pd-t15 z-pd-r30 z-mg0 z-rem18 z-text-ellipsis-line" style="background:'+(this.titleBgColor||'none')+'">'+this.title+'</h4>';
        var content = '<div class="z-rem14 z-overflow-x-hidden z-text-center z-overflow-y-hidden" style="max-height:4rem;max-height:80vw;">'+(this.hasTitle?title:'')+'</div>';
        var shadow = '<div class="z-absolute z-t0 z-r0 z-b0 z-l0 z-w100p z-h100p" style="z-index:9998;background:'+(this.shadowColor||'none')+'"></div>';

        this.$dialog = Z(dialog).appendTo("body");
        this.$content = Z(content).appendTo(this.$dialog);
        this.$title = this.$content.find("h4");

        if (this.shadow){
            this.$shadow = Z(shadow).appendTo("body");
            this.$shadow.fadeTo(500, this.shadowOpacity);
        }

        Z.Dialog.cache.put(this.id, this);

        //3.取消活动焦点对象
        this.active = document.activeElement;
        if (this.active != null && this.active.blur)
            this.active.blur();

        //4.组装内容
        this.$content[0].insertAdjacentHTML("beforeend",this.text);

        //5.设置状态并回调
        this.status = 1;

        if(Z.T.isFunction(this.callback))
        {//如果有打开回调函数
            this.callback.call(this, this.status);
        }

        if(this.timeout > 0)
        {//如果设置了定时关闭,增加定时器
            var that = this;
            var timer = setTimeout(function(){
                that.$dialog.fadeOut(that.timeout, Z.bind(that.close, that));
            },1000);
        }
    },

    close: function(e, callback)
    {//关闭弹出层,e可能是事件，也可能是立即关闭
        //1.阻止冒泡和缺省事件，以便不影响上层如$title.click等事情
        if (e instanceof Event){
            Z.E.forbidden(e);
        }

        //2.已关闭不再处理
        if (this.status == 0)
            return;

        //3.从静态缓存中删除该窗口
        Z.Dialog.cache.remove(this.id);

        //4.删除窗口和遮罩层
        this.remove(e === true);

        //5.来自业务层的回调
        if (Z.T.isFunction(callback)){
            callback.call(this, e);
        }
    },

    remove: function(immediate)
    {//删除弹出层，如本身close调用或由上层Z.Dialog.remove调用
        //1.已关闭不再处理
        if (this.status == 0)
            return;

        //2.删除窗口本身
        this.$dialog.remove();

        //3.置当前窗口为焦点窗口
        window.focus();

        //4.删除遮罩层
        if (this.shadow){
            if (immediate)
                this.$shadow.remove();
            else
                this.$shadow.fadeOut(500, function(){Z(this).remove();});
        }
        //5.设置为已关闭
        this.status = 0;

        //6.如果有回调函数则回调
        if (Z.T.isFunction(this.callback)){
            this.callback.call(this, this.status);
        }
    },
};

Z.dialog = function(param)
{
    var dialog = new Z.Dialog();
    for (var key in param){
        dialog.set(key, param[key]);
    }
    dialog.execute();
};

/************************************************/
//以下为警告、询问、加载中三个专属对话框
/************************************************/

Z.alert = function(text, callback, param)
{//警告
    var type = param && param.type || "alert";
    var content = '<div class="z-flexBox z-justify-center z-bg-white z-pd20 z-rem16">'+text+'</div>'
    content += '<div class="z-flexBox"><div class="z-button-flex">确定</div></div>';

    var dialog = new Z.Dialog();
    dialog.title = type=="success"?"成功":type=="failure"?"失败":"注意";
    dialog.hasTitle = false;
    dialog.text = content;
    dialog.execute();
    
    dialog.$content.find(".z-button-flex").on("touchstart",function(e){dialog.close(e, callback);});
};

Z.confirm = function(text, callback, param)
{//询问
    var content = '<div class="z-flexBox z-justify-center z-bg-white z-pd20 z-rem16">'+text+'</div>';
    content += '<div class="z-button-row"><div class="z-button-flex z-blue-bd">确定</div><div class="z-button-flex z-red-bd">取消</div></div>';

    var dialog = new Z.Dialog();
    dialog.hasBackground = param && param.hasBackground || false;
    dialog.title = "询问";
    dialog.hasTitle = false;
    dialog.text = content;
    dialog.execute();

    dialog.$content.css("overflow", "hidden");
    dialog.$content.find(".z-button-row").find(".z-red-bd").focus().on("touchend",function(e){dialog.close(e);});
    dialog.$content.find(".z-button-row").find(".z-blue-bd").on("touchend",function(e){dialog.close(e, callback);});

    //设置后面的代码不执行
    throw "stop";
};

Z.prompt = function(text, value, callback, param)
{//修改
    var isTextarea = param && param.type == "textarea";
    var width = param && param.width || Z.Dialog.innerWidth();;
    var height = param && param.height || (isTextarea)?150:120;
    var content = '<div class="z-container"><div class="z-prompt">';
    if (isTextarea)
        content += '<textarea class="z-textarea" spellcheck="false">'+value+'</textarea>';
    else
    {
        content += '<input class="z-input z-large" type="text" value="'+value+'" spellcheck="false">';
        Z(document).keydown(entry);
    }
    content += '</div></div><div class="z-console"><div class="z-button z-blue z-ok">确定</div><div class="z-button z-cancel">取消</div></div>';

    var dialog = new Z.Dialog();
    dialog.fixed = true;
    dialog.hasBackground = param && param.hasBackground || false;
    dialog.title = Z.S.toString(text || "提示修改");
    dialog.text = content;
    dialog.width = width;
    dialog.height = height;
    dialog.execute();

    dialog.$dialog.css("position", "fixed");
    dialog.$content.css("overflow", "hidden");
    dialog.$content.find(".z-console").find(".z-cancel").click(function(){dialog.close();});

    //输入数据
    var $input = dialog.$content.find(".z-container").find(".z-prompt").find(isTextarea?"textarea":"input").focusEnd();

    function ok()
    {
        dialog.close();
        if (Z.T.isFunction(callback)){callback($input.val());};
    }
    dialog.$content.find(".z-console").find(".z-ok").click(ok);

    //控制回车关闭
    function entry(e)
    {
        if (Z.E.key(e) != Z.E.KEY.ENTER)
            return;

        Z(document).offkeydown(entry);
        ok();
    }


    //设置后面的代码不执行
    throw "stop";
}

Z.loading = function(param)
{//加载中
    var text = "正在加载...";
    if (Z.T.isString(param)){
        text = param;
        param = null;
    }

    text = param && param.text || text;
    var radius = param && param.radius || "z-bd-rd5";

    var icoHtml = '<div class="z-w35 z-h35" style="-webkit-animation:rotate360 1s steps(12, end) infinite;animation:rotate360 1s steps(12, end) infinite;">';
    icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
    icoHtml += '<path d="M781.0103717 900.0688549c10.86520093 18.36326115 4.74450261 42.84722176-14.23117881 53.17568398-18.66947129 10.86520093-42.31048192 4.43829248-52.86947385-14.23117881-11.40077682-18.36326115-4.74333753-42.31164701 13.61992363-53.17568398C746.81036914 875.05048349 770.14633472 881.70675769 781.0103717 900.0688549L781.0103717 900.0688549 781.0103717 900.0688549zM551.32173653 983.08542123L551.32173653 983.08542123c0 21.19366315-17.52147627 38.94450517-39.55576035 38.94450517-21.19482709 0-38.94450517-17.29211051-38.94450517-38.94450517l0-27.31436146c0-21.72923904 17.82652131-39.02134841 38.94450517-39.02134841 21.72807395 0 39.55576035 17.29094542 39.55576035 39.02134841L551.32173653 983.08542123 551.32173653 983.08542123 551.32173653 983.08542123zM310.76790045 939.09020445L310.76790045 939.09020445c-11.09456782 19.2038821-35.04178859 25.32574549-53.48189298 14.46054456-18.8976731-10.63583517-25.63195563-34.50621269-14.45938062-53.48189298l28.99676842-50.72717141c11.17141106-18.66947129 35.11979577-25.09637973 54.01863282-14.46054457 18.36326115 10.86403698 25.09521579 35.11979577 13.92496868 53.48189298L310.76790045 939.09020445 310.76790045 939.09020445 310.76790045 939.09020445zM124.15468885 781.47609031L124.15468885 781.47609031c-18.8976731 10.55899193-43.15110286 4.20776277-53.48072903-14.46054457-11.09456782-18.36326115-4.74450261-42.61669205 13.92496868-53.48072903l77.96585358-45.14204786c18.66947129-10.32962617 42.84722176-4.51397177 53.48189298 14.45938062 10.55782798 18.36326115 4.1309184 42.617856-14.46054457 53.48189298L124.15468885 781.47609031 124.15468885 781.47609031 124.15468885 781.47609031zM41.21613085 551.17387093L41.21613085 551.17387093c-21.72923904 0-38.94450517-17.52147627-38.94450518-38.94450517 0-21.72807395 17.21526613-39.24955022 38.94450518-39.24955022l121.42433052 0c21.72923904 0 38.94450517 17.52147627 38.94450517 38.94450517 0 21.72807395-17.21526613 39.24955022-38.94450517 39.24955022L41.21613085 551.17387093 41.21613085 551.17387093 41.21613085 551.17387093zM84.67460893 310.31498979L84.67460893 310.31498979c-18.97451634-10.55899193-25.09521579-34.50854059-13.92496868-53.17568398 10.32962617-18.89883705 34.50621269-25.32691058 53.48189298-14.46170851l131.90531527 76.5884928c18.66947129 10.63583517 24.79017074 34.50621269 14.46054457 52.94631708-11.09456782 18.89883705-35.04178859 25.02070045-53.40504974 14.46054457L84.67460893 310.31498979 84.67460893 310.31498979 84.67460893 310.31498979zM242.8254629 123.77745749L242.8254629 123.77745749l91.89082225 159.52821476c11.17024711 18.66947129 35.11979577 25.32458155 53.71125988 14.23117881 18.66947129-10.55782798 24.79017074-34.81358677 13.92613262-53.48189298l-92.19703239-159.22084181c-10.55899193-18.43894045-34.50854059-25.09521579-52.87063779-14.23117881C238.31149113 81.46581163 232.19079168 105.41419634 242.8254629 123.77745749L242.8254629 123.77745749 242.8254629 123.77745749zM472.82147101 41.37447538L472.82147101 41.37447538c0-21.19482709 17.82652131-39.25187925 38.94450517-39.25187925 21.72807395 0 39.55576035 17.5983195 39.55576035 39.25187925l0 183.78164565c0 21.72923904-17.52147627 39.25187925-39.55576035 39.55576036-21.19482709 0-38.94450517-17.21526613-38.94450517-39.55576036L472.82147101 41.37447538 472.82147101 41.37447538 472.82147101 41.37447538zM713.37530823 84.83295232L713.37530823 84.83295232c11.17024711-18.8976731 34.81358677-25.09521579 53.48072903-14.46054457 18.89883705 10.55899193 25.63195563 34.50854059 14.46054457 53.48189298l-92.1213531 159.52821476c-10.40530432 18.59262805-34.88926606 25.24890226-53.48072903 14.46054456-18.66947129-10.86403698-24.79017074-35.11979577-14.23234276-53.78810311L713.37530823 84.83295232 713.37530823 84.83295232 713.37530823 84.83295232zM900.52409458 242.6775973L900.52409458 242.6775973l-159.83442489 92.1213531c-18.66947129 10.55782798-25.09637973 34.50621269-14.46054457 53.48072903 11.09456782 18.36326115 35.04295253 24.48396061 53.48189298 14.23234276l159.83442489-92.1213531c18.36326115-10.55899193 25.09521579-34.50621269 13.92496867-53.17568399C942.8345765 238.23930482 918.8105125 231.50735019 900.52409458 242.6775973L900.52409458 242.6775973 900.52409458 242.6775973zM982.61970261 472.97865159L982.61970261 472.97865159c21.95976875 0 39.25187925 17.52147627 38.94450518 39.24955022 0 21.42302891-16.98473643 38.94450517-38.94450518 38.94450518L798.6086912 551.17270699c-21.19366315 0-39.02134841-17.52147627-39.02134841-39.24955022 0-21.42302891 17.8276864-38.94450517 39.02134841-38.94450518L982.61970261 472.97865159 982.61970261 472.97865159 982.61970261 472.97865159z" ';
    icoHtml += 'fill="#ffffff"/></svg>';
    icoHtml += '</div>';
    var content = '<div class="z-flexBox z-direction-column z-flex-center z-pd-t10 z-pd-r20 z-pd-l20 zi-bg-none">'+icoHtml + '<div class="z-flex-none z-mg-t3 z-color-white">' + text+'</div></div>';
    var dialog = new Z.Dialog();
    dialog.hasTitle = false;
    dialog.shadow = param && param.shadow || false;
    dialog.text = content;
    dialog.execute();
    dialog.$content.addClass(radius).addClass("z-flex-none");
    if (!dialog.shadow) {
        dialog.$content.css("background","rgba(85,85,85,.6)");
    }
    return dialog;
};

Z.tips = function(param)
{//提示
    var text = "处理成功";
    if (Z.T.isString(param)){
        text = param;
        param = null;
    }

    text = param && param.text || text;
    var radius = param && param.radius || "z-bd-rd5";
    var color = param && param.color || "#fff";
    var background = param && param.background || "#333";
    var ico = param && param.ico || "finish";
    var timeout = param && param.timeout || 300;

    var icoHtml = '<div class="z-w18 z-h18 z-mg-r5 z-mg-l5">';
    switch (ico){
        case "finish":
            icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
            icoHtml += '<path d="M512 1024a512 512 0 1 1 512-512 512 512 0 0 1-512 512z m248.604444-721.351111l-316.302222 316.871111-181.475555-180.906667-44.942223 45.511111 226.417778 225.848889 361.813334-361.813333z" ';
            icoHtml += 'fill="#ffffff"/></svg>';
            break;
        case "failure":
            icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
            icoHtml += '<path d="M1024 512c0-282.763636-229.236364-512-512-512C229.236364 0 0 229.236364 0 512c0 282.763636 229.236364 512 512 512C794.763636 1024 1024 794.763636 1024 512zM281.6 709.492364 479.092364 512 281.6 314.507636 314.507636 281.6 512 479.092364l197.492364-197.492364 32.907636 32.907636L544.907636 512l197.492364 197.492364-32.907636 32.907636L512 544.907636 314.507636 742.4 281.6 709.492364z" ';
            icoHtml += 'fill="#ffffff"/></svg>';
            break;
        case "alert":
            icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
            icoHtml += '<path d="M512 2.27555555C230.74133333 2.27555555 2.27555555 230.74133333 2.27555555 512s228.46577778 509.72444445 509.72444445 509.72444445 509.72444445-228.46577778 509.72444445-509.72444445S793.25866667 2.27555555 512 2.27555555z m36.4088889 800.99555556l-72.81777778 0 0-72.81777778 72.81777778 0 0 72.81777778z m-72.81777778-145.63555556L475.59111112 220.72888889l72.81777778 0-1e-8 436.90666667-72.81777777-1e-8z" ';
            icoHtml += 'fill="#ffffff"/></svg>';
            break;
        case "confirm":
            icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
            icoHtml += '<path d="M512.026875 0C229.23903 0 0 229.23903 0 512.026875 0 794.77597 229.23903 1024 512.026875 1024c282.779095 0 512.003125-229.22403 512.003125-511.973125C1024.03 229.23903 794.80597 0 512.026875 0zM504.204365 753.135919c-13.002516 0-23.583779-10.576263-23.583779-23.577529 0-13.003766 10.581263-23.580029 23.583779-23.580029 13.003766 0 23.580029 10.576263 23.580029 23.580029C527.784394 742.559656 517.208131 753.135919 504.204365 753.135919zM588.466968 530.593148c-37.261295 18.812523-69.438835 35.062543-69.438835 73.90009l0 59.946323c0 8.19001-6.655008 14.843768-14.838768 14.843768-8.17876 0-14.830018-6.653758-14.830018-14.843768l0-59.946323c0-57.11007 45.567556-80.101348 85.767605-100.386373 41.24505-20.825025 80.181348-40.487549 80.181348-93.510114 0-31.715039-14.805018-60.161323-41.685051-80.102598-26.411282-19.600024-62.162576-29.956287-103.392626-29.956287-72.232588 0-120.465147 43.617553-136.231416 86.850106-4.897506 13.447516-6.655008 30.293787-4.272505 40.96755 1.777502 7.99251-3.251254 15.946269-11.211264 17.723772-1.058751 0.235-2.137503 0.3525-3.217504 0.3525-6.918758 0-13.032516-4.873756-14.531268-11.582514-4.101255-18.388772-0.26375-42.192552 5.356257-57.62757 23.203778-63.612578 89.151359-106.35763 164.1077-106.35763 102.890126 0 174.750213 57.46132 174.745213 139.732671C684.975836 481.919338 633.702024 507.77937 588.466968 530.593148z" ';
            icoHtml += 'fill="#ffffff"/><path d="M512.026875 999.03872" fill="#ffffff"/></svg>';
            break;
    }
    icoHtml += '</div>';

    var content = '<div class="z-flexBox z-justify-center z-pd-lr5 z-pd-tb8" style="background-color:'+background+';">'+icoHtml + '<div class="z-flex-none">' + text+'</div></div>';
    var dialog = new Z.Dialog();
    dialog.hasTitle = false;
    dialog.shadow = param && param.shadow || false;
    dialog.text = content;
    dialog.timeout = timeout;
    dialog.execute();

    dialog.$dialog.addClass("z-event-none");
    dialog.$content.css("overflow", "hidden")
        .addClass(radius).addClass("z-flex-none")
        .css("background", background).css("color", color);
    return dialog;
};

//END
})(zhiqim);