/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 

+(function(Z)
{
/****************************************/
//“mobile 滑倒底部刷新”
/****************************************/
Z.ScrollLoad = Z.Class.newInstance();
Z.ScrollLoad.prototype = 
{
    //start of Z.ScrollLoad.prototype
    defaults: 
    {//定义
        elem: "",              // 刷新列表的容器
        ajaxClass: "",         // ajax 类名
        ajaxMethod: "",        // ajax 方法名
        ajaxParams: null,      // ajax 参数组
        paramPage: "page",     // 页码的关键词
        ajaxSuccess: null,     // 成功的回调
        ajaxFailue: null,     // 失败的回调
        
        loading: false,        // 正在刷新的标识
        loadPage: 2,           // 页码
        distance: 30,          // 触发事件的临界高度
    },
    
    validate: function()
    {//验证
        //滚动容器
        if (!Z("body>.z-container")[0]){
            return false;
        }
        this.$scroller = Z("body>.z-container");
        
        //不存在 scroll 高度
        this.sOffsetHeight = this.$scroller[0].offsetHeight;
        var sScrollHeight = this.$scroller[0].scrollHeight;
        if (this.sOffsetHeight >= sScrollHeight){
            return false;
        }
        
        //ajax 参数
        if (!this.ajaxClass){
            Z.alert("Z.ScrollLoad 参数 ajaxClass 配置有误");
            return false;
        }
        if (!this.ajaxMethod){
            Z.alert("Z.ScrollLoad 参数 ajaxMethod 配置有误");
            return false;
        }
        if (typeof this.ajaxParams != "object"){
            Z.alert("Z.ScrollLoad 参数 ajaxParams 配置有误");
            return false;
        }
        
        return true;
    },
    
    execute: function()
    {//执行
        //1： 验证
        if (!this.validate()){
            return;
        }
        
        //2: 初始化基本参数
        this.$elem = Z.$elem(this.elem, "Z.ScrollLoad");
        
        /* 强制 loading 为 false */
        this.loading = false;
        
        /* 计算 distance */
        this.distance = +this.distance;
        if (this.distance > this.sOffsetHeight || this.distance < 0)
            this.distance = 30;
        
        /* 加载图标对象 $loading */
        var icoHtml = '<div class="z-w100p z-pd15 z-text-center zi-bg-none" style="display:block;"><div class="z-show-ib z-w35 z-h35" style="-webkit-animation:rotate360 1s steps(12, end) infinite;animation:rotate360 1s steps(12, end) infinite;">';
        icoHtml += '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1025 1024" version="1.1" width="100%" height="100%">';
        icoHtml += '<path d="M781.0103717 900.0688549c10.86520093 18.36326115 4.74450261 42.84722176-14.23117881 53.17568398-18.66947129 10.86520093-42.31048192 4.43829248-52.86947385-14.23117881-11.40077682-18.36326115-4.74333753-42.31164701 13.61992363-53.17568398C746.81036914 875.05048349 770.14633472 881.70675769 781.0103717 900.0688549L781.0103717 900.0688549 781.0103717 900.0688549zM551.32173653 983.08542123L551.32173653 983.08542123c0 21.19366315-17.52147627 38.94450517-39.55576035 38.94450517-21.19482709 0-38.94450517-17.29211051-38.94450517-38.94450517l0-27.31436146c0-21.72923904 17.82652131-39.02134841 38.94450517-39.02134841 21.72807395 0 39.55576035 17.29094542 39.55576035 39.02134841L551.32173653 983.08542123 551.32173653 983.08542123 551.32173653 983.08542123zM310.76790045 939.09020445L310.76790045 939.09020445c-11.09456782 19.2038821-35.04178859 25.32574549-53.48189298 14.46054456-18.8976731-10.63583517-25.63195563-34.50621269-14.45938062-53.48189298l28.99676842-50.72717141c11.17141106-18.66947129 35.11979577-25.09637973 54.01863282-14.46054457 18.36326115 10.86403698 25.09521579 35.11979577 13.92496868 53.48189298L310.76790045 939.09020445 310.76790045 939.09020445 310.76790045 939.09020445zM124.15468885 781.47609031L124.15468885 781.47609031c-18.8976731 10.55899193-43.15110286 4.20776277-53.48072903-14.46054457-11.09456782-18.36326115-4.74450261-42.61669205 13.92496868-53.48072903l77.96585358-45.14204786c18.66947129-10.32962617 42.84722176-4.51397177 53.48189298 14.45938062 10.55782798 18.36326115 4.1309184 42.617856-14.46054457 53.48189298L124.15468885 781.47609031 124.15468885 781.47609031 124.15468885 781.47609031zM41.21613085 551.17387093L41.21613085 551.17387093c-21.72923904 0-38.94450517-17.52147627-38.94450518-38.94450517 0-21.72807395 17.21526613-39.24955022 38.94450518-39.24955022l121.42433052 0c21.72923904 0 38.94450517 17.52147627 38.94450517 38.94450517 0 21.72807395-17.21526613 39.24955022-38.94450517 39.24955022L41.21613085 551.17387093 41.21613085 551.17387093 41.21613085 551.17387093zM84.67460893 310.31498979L84.67460893 310.31498979c-18.97451634-10.55899193-25.09521579-34.50854059-13.92496868-53.17568398 10.32962617-18.89883705 34.50621269-25.32691058 53.48189298-14.46170851l131.90531527 76.5884928c18.66947129 10.63583517 24.79017074 34.50621269 14.46054457 52.94631708-11.09456782 18.89883705-35.04178859 25.02070045-53.40504974 14.46054457L84.67460893 310.31498979 84.67460893 310.31498979 84.67460893 310.31498979zM242.8254629 123.77745749L242.8254629 123.77745749l91.89082225 159.52821476c11.17024711 18.66947129 35.11979577 25.32458155 53.71125988 14.23117881 18.66947129-10.55782798 24.79017074-34.81358677 13.92613262-53.48189298l-92.19703239-159.22084181c-10.55899193-18.43894045-34.50854059-25.09521579-52.87063779-14.23117881C238.31149113 81.46581163 232.19079168 105.41419634 242.8254629 123.77745749L242.8254629 123.77745749 242.8254629 123.77745749zM472.82147101 41.37447538L472.82147101 41.37447538c0-21.19482709 17.82652131-39.25187925 38.94450517-39.25187925 21.72807395 0 39.55576035 17.5983195 39.55576035 39.25187925l0 183.78164565c0 21.72923904-17.52147627 39.25187925-39.55576035 39.55576036-21.19482709 0-38.94450517-17.21526613-38.94450517-39.55576036L472.82147101 41.37447538 472.82147101 41.37447538 472.82147101 41.37447538zM713.37530823 84.83295232L713.37530823 84.83295232c11.17024711-18.8976731 34.81358677-25.09521579 53.48072903-14.46054457 18.89883705 10.55899193 25.63195563 34.50854059 14.46054457 53.48189298l-92.1213531 159.52821476c-10.40530432 18.59262805-34.88926606 25.24890226-53.48072903 14.46054456-18.66947129-10.86403698-24.79017074-35.11979577-14.23234276-53.78810311L713.37530823 84.83295232 713.37530823 84.83295232 713.37530823 84.83295232zM900.52409458 242.6775973L900.52409458 242.6775973l-159.83442489 92.1213531c-18.66947129 10.55782798-25.09637973 34.50621269-14.46054457 53.48072903 11.09456782 18.36326115 35.04295253 24.48396061 53.48189298 14.23234276l159.83442489-92.1213531c18.36326115-10.55899193 25.09521579-34.50621269 13.92496867-53.17568399C942.8345765 238.23930482 918.8105125 231.50735019 900.52409458 242.6775973L900.52409458 242.6775973 900.52409458 242.6775973zM982.61970261 472.97865159L982.61970261 472.97865159c21.95976875 0 39.25187925 17.52147627 38.94450518 39.24955022 0 21.42302891-16.98473643 38.94450517-38.94450518 38.94450518L798.6086912 551.17270699c-21.19366315 0-39.02134841-17.52147627-39.02134841-39.24955022 0-21.42302891 17.8276864-38.94450517 39.02134841-38.94450518L982.61970261 472.97865159 982.61970261 472.97865159 982.61970261 472.97865159z" ';
        icoHtml += 'fill="#999999"/></svg>';
        icoHtml += '</div></div>';
        this.$loading = Z(icoHtml).insertAfter(this.$elem);
        
        //3：绑定事件
        this.$scroller.on("scroll", this.scroll, this);
    },
    
    scroll: function()
    {//主事件
        //加载中
        if (this.loading == true ){
            return;
        }
        
        //临界值
        var scrollTop = this.$scroller[0].scrollTop;
        var scrollHeight = this.$scroller[0].scrollHeight;
        if (scrollTop + this.sOffsetHeight < scrollHeight - this.distance){
            return;
        }
        
        //开始执行
        this.loading = true;
        
        //调用 ajax
        this.doAjax();
    },
    
    doAjax: function()
    {//ajax
        this.ajax = new Z.Ajax();
        
        //方法
        this.ajax.setClassName(this.ajaxClass);
        this.ajax.setMethodName(this.ajaxMethod);
        
        //参数
        this.ajax.addParam(this.paramPage, this.loadPage);
        this.addParams();
        
        //回调
        var that = this;
        this.ajax.setSuccess(function(){
            that.success.call(that, this.responseText);
        });
        this.ajax.setFailure(function(){
            that.failure.call(that, this.responseText);
        });
        
        //执行
        this.ajax.execute();
    },
    
    addParams: function()
    {//添加参数
        if (!this.ajaxParams){
            return;
        }
        for (var i in this.ajaxParams)
        {
            var thisValue = this.ajaxParams[i];
            if (typeof thisValue == "function"){
                this.ajax.addParam(i, thisValue());
                continue;
            }
            this.ajax.addParam(i, thisValue);
        }
    },
    
    success: function(responseText)
    {//ajax成功
        if (Z.V.isEmptyBlank(responseText)){
            return this.$loading.hide();
        }
        
        //翻页加一
        this.loadPage++;
        
        //执行成功处理方法
        var listHtml = this.ajaxSuccess;
        if (typeof this.ajaxSuccess == "function"){
            listHtml = this.ajaxSuccess(responseText);
        }
        this.$elem[0].insertAdjacentHTML("beforeend",listHtml);
        
        //touchend 事件
        Z.ResetTouchEnd.load(this.$elem);
        
        //滚动的标识，延迟 500ms
        var that = this;
        setTimeout(function(){
            that.loading = false;
        },500);
    },
    
    failure: function(responseText)
    {//ajax失败
        if (typeof this.ajaxSuccess == "function"){
            this.ajaxFailue(responseText);
        }
    },
    //end of Z.ScrollLoad.prototype
}
//END
})(zhiqim);


