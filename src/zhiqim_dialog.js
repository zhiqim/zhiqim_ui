/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 *
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

+(function(Z)
{//BEGIN
// @version v1.1.0 @author zouzhigang 2015-11-12 新建与整理

// 调用举例:
//     1)直接调用并执行、格式如Z.dialog({title:"提示",url:"/index.htm", width: 600, height: 400});
//     2)设置参数后执行：
//         var dialog = new Z.Dialog();
//         dialog.tile = "提示";
//         dialog.url = "/index.htm";
//         dialog.width = 600;
//         dialog.height = 400;
//         dialog.execute();
//
// 基本参数说明如下：
//         1)id               表示该窗口的唯一编号，随机生成，可以通过Z.D.id(id)获取元素
//         2)title            表示该窗口标题，默认"提示"
//         3)width            表示该窗口宽度，默认300
//         4)height           表示该窗口高度，默认200
//         5)borderColor      表示边框颜色，默认#A6C9E1
//         6)target           表示该窗口在目标区域的位置，默认document，可以传入element
//         7)fixed            表示该窗口是否在document固定位置，默认false
//         8)position         表示该窗口位置，默认为null，可以传入对象{top:100,left:100}或{right:100,bottom:100}，或者指定的9个位置
//                            1 ---- 4 ---- 6
//                            |             |
//                            |             |
//                            2      0      7
//                            |             |
//                            |             |
//                            3 ---- 5 ---- 8
//         9)timeout          表示该窗口定时关闭，单位毫秒
//         10)callback        表示该窗口被执行时的回调函数
// 内容相关2个参数
//         11)url             表示该窗口为加载的地址
//         12)text            表示该窗口为加载的文本内容
// 遮罩层参数说明如下：
//         13)shadow          表示遮罩层是否启用 true|false 默认true
//         14)shadowColor     表示遮罩层颜色，默认#757575
//         15)shadowOpacity   表示遮罩层透明度，值在0-1之间，默认0.6
// 标题拖拽说明如下：
//         16)hasTitle        表示标题栏是否显示，true|false 默认true，当=false时drag无意义
//         17)titleBgColor    表示标题背景色
//         18)drag            表示是否支持拖拽标题，true|false 默认true
//         19)dragOpacity     表示拖拽时透明度，值在0-1之间，默认0.8
// 背景参数说明如下：
//         20)hasBackground   表示背景是否显示，true|false 默认true
//         21)backgroundColor 表示外边框颜色，默认#d5d5d5
// 滚动滚动条如下：
//         21)scroll          表示是否滚动，true|false 默认false
//         22)scrollHeight    表示指定滚动高度，(scrollHeight==0)表示到底部，即最大高度，!=0表示指定高度
//         23)scrollPad       表示指定滚动高度时，保留一部分即height=scrollHeight-scrollPad
// 自定义状态参数说明如下：
//         24)status          表示该窗口状态，0表示未开启，1表示已开启
///////////////////////////////////////////////////////////////////////////////////////

Z.Dialog = Z.Class.newInstance();
Z.Dialog.v = "8.0.4";
/****************************************/
//定义Z.Dialog下的静态属性和关闭所有的方法
/****************************************/
Z.Dialog.cache = new Z.HashMap();
Z.Dialog.close = function(id, immediate)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    if (!id)
    {//删除全部
        Z.each(Z.Dialog.cache.values(), function(dialog){dialog.remove(immediate);});
        Z.Dialog.cache.clear();
    }
    else
    {//找到id删除
        var dialog = Z.Dialog.cache.get(id);
        if (dialog)
        {
            dialog.remove(immediate);
            Z.Dialog.cache.remove(id);
        }
    }
};
Z.Dialog.get = function(id)
{
    id = id?((id.charAt(0)=="#")?id.substring(1):id):id;
    return Z.Dialog.cache.get(id);
};

/****************************************/
//定义Z.Dialog下的原型属性和方法
/****************************************/
Z.Dialog.prototype =
{
    defaults:
    {
        //基础参数9个参数
        id: null,
        title:"提示",
        width: 300,
        height: 200,
        borderColor:"#ccc",
        target: document,
        fixed: false,
        position: null,
        timeout: 0,
        callback: null,

        //内容相关2个参数
        text: null,
        url: null,

        //背景控制2个参数
        hasBackground:true,
        backgroundColor:"#d5d5d5",

        //标题控制3个参数
        hasTitle:true,
        titleBgColor: "#f5f5f5",
        drag: true,
        dragOpacity:0.8,

        //以下3个参数为[遮罩层]参数
        shadow: true,
        shadowColor:"#757575",
        shadowOpacity: 0.6,

        //控制滚动条3个参数
        scroll: false,
        scrollHeight: 0,
        scrollPad: 0,

        //自定义内部参数
        status: 0
    },

    init: function()
    {
        if (!this.id)
        {//没传入id则随机新建
            this.id = Z.random(10);
        }
    },

    validate: function()
    {
        if (Z.D.has("Z_Dialog_"+this.id))
        {
            alert("[Z.Dialog]您传入了相同的窗口ID,如无必须可以不传入由系统随机生成");
            return false;
        }

        if (!this.text && !this.url)
        {
            alert("[Z.Dialog]您需传入的text或url参数值");
            return false;
        }

        if (!this.target && !Z.T.isString(this.target) && !this.target.nodeType)
        {
            alert("[Z.Dialog]您传的[target]格式不正确，仅支持document/element和字符串");
            return false;
        }

        if (Z.T.isString(this.target))
        {
            if (!Z.D.has(this.target))
            {
                alert("[Z.Dialog]您传的[target]不存在");
                return false;
            }
            this.target = Z.D.id(this.target);
        }

        this.width = parseInt(this.width);
        this.height = parseInt(this.height);

        return true;
    },

    execute: function()
    {
        //1.检查参数
        if (!this.validate())
            return;

        this.isDocumentTarget = Z.T.isDocument(this.target);
        //2.打开对话框和加载内容，放置到body和cache中
        var dialog = '<div class="z-dialog" id="Z_Dialog_'+this.id+'" tabindex = "-1" style = "outline:none;">';
        dialog +=    '    <div class="z-background z-bd-rd3"></div>';
        dialog +=    '    <div class="z-wrap z-bd-rd3">';
        dialog +=    '        <div class="z-title z-bd-rd-tl3 z-bd-rd-tr3"><span></span><i class="z-font z-error"></i></div>';
        dialog +=    '        <div class="z-content z-bd-rd-bl3 z-bd-rd-br3"></div>';
        dialog +=    '    </div>';
        dialog +=    '</div>';

        this.shadow = this.shadow === true;
        this.$dialog = Z(dialog);
        if (this.isDocumentTarget)
            this.$dialog.appendTo("body");
        else
            this.$dialog.appendToPos(this.target);
        Z.Dialog.cache.put(this.id, this);

        //3.取消活动焦点对象
        this.active = document.activeElement;
        if (this.active != null && this.active.blur)
            this.active.blur();

        /**************************************************/
        //第三步，设置对话框CSS
        /**************************************************/

        var totalWidth = this.width + (this.hasBackground?10:0);
        var totalHeight = this.height + (this.hasBackground?10:0) + (this.hasTitle?40:0);
        var position = this.calcPosition(totalWidth, totalHeight);

        //3.1 窗体
        this.$dialog.css(position);
        this.$dialog.css({width: totalWidth,height: totalHeight});

        //3.2 背景和实体
        this.$background = this.$dialog.find(".z-background");
        this.$wrap = this.$dialog.find(".z-wrap");
        if(!this.hasBackground)
        {//无背景时删除背景，实体100%
            this.$background.remove();
            this.$wrap.css({top: 0, left: 0, width: totalWidth, height: totalHeight});
        }
        else
        {//有背景时设置背景颜色，实体上下左右留5px背景空间
            this.$background.css("backgroundColor", this.backgroundColor);
            this.$wrap.css({top: 5, left: 5, width: (totalWidth-10), height: (totalHeight-10)});
        }

        //3.3 标题栏和内容
        this.$title = this.$wrap.find(".z-title");
        this.$content = this.$wrap.find(".z-content");
        this.$content.css({height: this.height,borderColor: this.borderColor});
        if(!this.hasTitle)
        {//无标题删除标题，内容100%
            this.$title.remove();
            this.$content.css("top", 0);
        }
        else
        {//有标题时设置标题和关闭按钮点击、移入事件
            this.$title.css({borderColor:this.borderColor,backgroundColor:this.titleBgColor});
            this.$title.find("span").html(this.title);
            this.$title.find("i").click(this.close, this).mouseover(function(e){Z.E.stop(e);});//停止冒泡到$title中，防止手势变成移动
        }

        //3.4 只有全屏时固定位置
        if (this.fixed && this.target === document){
            this.$dialog.css("position", "fixed");
        }

        /**************************************************/
        //第四步，组装内容
        /**************************************************/
        if (this.text)
        {//文本
            this.$content.html(this.text);
        }
        else
        {//URL
            var url = this.url+((this.url.indexOf("?")==-1)?"?":"&")+"zDialogFrame=true"
            this.$content.css({overflowY:"hidden"});
            this.$content.append("<iframe id=Z_Dialog_frame_"+this.id+" name=Z_Dialog_frame_"+this.id+" src='"+url+"#"+this.id+"' width='100%' height='100%' scrolling='auto' frameborder='0' marginheight='0' marginwidth='0'></iframe>");
        }

        /**************************************************/
        //第五步，设置其他属性
        /**************************************************/
        if (this.scroll)
        {//如果设置滚动
            this.doScroll();
        }

        if(this.shadow)
        {//如果设置遮罩层
            this.$shadow = Z('<div class="z-dialog-shadow" tabindex = "-1" style = "outline:none;"></div>');
            if (this.isDocumentTarget) {
                this.$shadow.appendTo('body')
                .css({width: '100%', height: '100%'})
                .css({position: 'fixed'});
            }
            else {
                this.$shadow.appendToPos(this.target);
                var scrollWidth = Z(this.target).scrollWidth(),
                    scrollHeight = Z(this.target).scrollHeight();
                this.$shadow.css({width: scrollWidth, height: scrollHeight});
                // 同时给document添加一个resize事件，同步修改遮罩尺寸
                Z(window).resize(this.resize, this);
            }
            this.$shadow.css({left: 0, top: 0})
                        .css({backgroundColor: this.shadowColor})
                        .fadeTo(500, this.shadowOpacity);
        }

        if(this.drag && this.hasTitle)
        {//如果支持拖拽则标题拖拽
            this.$dialog.drag(this.$title, this.doDrag, this);
        }

        //设置状态并回调
        this.status = 1;
        if(Z.T.isFunction(this.callback))
        {//如果有打开回调函数
            this.callback.call(this, this.status);
        }

        if(this.timeout > 0)
        {//如果设置了定时关闭,增加定时器
            //setTimeout(Z.bind(this.fadeClose, this), this.timeout);
            this.$dialog.fadeOut(this.timeout, Z.bind(this.close, this));
        }
    },

    calcPosition: function(totalWidth, totalHeight)
    {//计算最终位置

        if (Z.T.isPlainObject(this.position))
        {//如果指定位置则采用指定的位置
            return this.position;
        }

        switch(this.position)
        {//0居中123左45中678右
            //左上
            case 1: return {left: '0', top: '0', marginLeft: 0, marginTop: 0};
            //左中
            case 2: return {left: '0', top: '50%', marginLeft: 0, marginTop: totalHeight * -.5};
            //左下
            case 3: return {left: '0', top: '100%', marginLeft: 0, marginTop: totalHeight * -1};
            //中上
            case 4: return {left: '50%', top: '0', marginLeft: totalWidth * -.5, marginTop: 0};
            //中下
            case 5: return {left: '50%', top: '100%', marginLeft: totalWidth * -.5, marginTop: totalHeight * -1};
            //右上
            case 6: return {left: '100%', top: '0', marginLeft: totalWidth * -1, marginTop: 0};
            //右中
            case 7: return {left: '100%', top: '50%', marginLeft: totalWidth * -1, marginTop: totalHeight * -.5};
            //右下
            case 8: return {left: '100%', top: '100%', marginLeft: totalWidth * -1, marginTop: totalHeight * -1};
            //居中
            default: return {left: '50%', top: '50%', marginLeft: totalWidth * -.5, marginTop: totalHeight * -.5};
        }
    },

    close: function(e, callback)
    {//关闭弹出层,e可能是事件，也可能是立即关闭
        //1.阻止冒泡和缺省事件，以便不影响上层如$title.click等事情
        if (e instanceof Event){
            Z.E.forbidden(e);
        }

        //2.已关闭不再处理
        if (this.status == 0)
            return;

        //3.从静态缓存中删除该窗口
        Z.Dialog.cache.remove(this.id);

        //4.删除窗口和遮罩层
        this.remove(e === true);

        //5.移除resize事件
        if (this.shadow) {
            Z(window).offresize(this.resize, this);
        }

        //6.来自业务层的回调
        if (Z.T.isFunction(callback)){
            callback.call(this, e);
        }
    },

    remove: function(immediate)
    {//删除弹出层，如本身close调用或由上层Z.Dialog.remove调用

        //1.已关闭不再处理
        if (this.status == 0)
            return;

        //2.删除窗口本身
        this.$dialog.remove();

        //3.置当前窗口为焦点窗口
        window.focus();

        //4.删除遮罩层
        if (this.shadow)
        {
            if (immediate)
                this.$shadow.remove();
            else
                this.$shadow.fadeOut(500, function(){Z(this).remove();});
            //移除resize事件
            Z(window).offresize(this.resize, this);
        }

        //5.设置为已关闭
        this.status = 0;

        //6.如果有回调函数则回调
        if (Z.T.isFunction(this.callback)){
            this.callback.call(this, this.status);
        }
    },

    resize: function()
    {
        if (!this.shadow || this.isDocumentTarget)
            return;

        var $target = Z(this.target),
            $shadow = Z(this.$shadow),
            scrollWidth = $target.scrollWidth(),
            scrollHeight = $target.scrollHeight(),
            clientWidth = $target.clientWidth(),
            clientHeight = $target.clientHeight(),
            nowWidth = $shadow.offsetWidth(),
            nowHeight = $shadow.offsetHeight(),
            setWidth = scrollWidth === nowWidth && nowWidth > clientWidth && clientWidth || scrollWidth,
            setHeight = scrollHeight === nowHeight && nowHeight > clientHeight && clientHeight || scrollHeight;
        this.resizeWidth = setWidth;
        this.resizeHeight = setHeight;
        this.resizeSet(setWidth, setHeight);
    },

    resizeSet: function(setWidth, setHeight)
    {
        if (this.resizeTimer)
            clearTimeout(this.resizeTimer);
        this.resizeTimer = setTimeout(function(){
            if (setWidth !== this.resizeWidth || setHeight !== this.resizeHeight)
                return this.resizeSet(this.resizeWidth, this.resizeHeight);
            this.$shadow.css({width: setWidth, height: setHeight});
        }.bind(this), 300);
    },

    doDrag: function(e, dragging)
    {//设置拖拽回调
        if (!this.dragOpacity)
            return;

        this.$dialog.children("div").css("opacity", dragging?this.dragOpacity:1);
    },

    doScroll: function()
    {//滚动滚动条，仅支持内部URL，暂时采用判断是否是http/https开头来判断是否内部URL
        if (this.text || /http(s?):\/\//i.test(this.url))
            return;

        var cWindow = this.$content.find("iframe")[0].contentWindow;
        var cDocument = cWindow.document;

        Z.E.add(cDocument, "DOMContentLoaded", this.doScrollLoaded, this);
        Z.E.add(cWindow, "load", this.doScrollLoaded, this);
    },

    doScrollLoaded: function(e)
    {
        var cWindow = this.$content.find("iframe")[0].contentWindow;

        var cDocument = cWindow.document;
        var height = this.scrollHeight;
        if (!height)
        {//没指定高度表示到底部,滚动条全高度-可视高度=滚动条剩余高度
            height = Z.D.scrollHeight(cDocument) - Z.D.clientHeight(cDocument);
        }

        height -= this.scrollPad;
        cWindow.scrollTo(0, height);

        Z.E.remove(cWindow, "DOMContentLoaded", this.doScrollLoaded, this);
        Z.E.remove(cDocument, "load", this.doScrollLoaded, this);
    }
};

Z.dialog = function(param)
{
    var dialog = new Z.Dialog();
    for (var key in param){
        dialog.set(key, param[key]);
    }
    dialog.execute();
};

/************************************************/
//以下为警告、询问、加载中三个专属对话框
/************************************************/

Z.Dialog.innerWidth = function()
{
    var width = 250;
    if (screen.width > 1440)
        width = 450;
    else if(screen.width > 1024)
        width = 350;
    else if(screen.width > 800)
        width = 300;

    //有可视宽度时以可视宽度比较
    var clientWidth = Z.D.clientWidth();
    return (clientWidth > 0)?Math.min(width, parseInt(clientWidth * 0.8)):width;

};

Z.alert = function(text, callback, param)
{//警告
    var width = param && param.width || Z.Dialog.innerWidth();
    var lineNum = Z.textLineNum(text, width-77, 14);

    var textHeight = lineNum==1?36:lineNum * 22;
    var lineHeight = lineNum==1?36:22;
    var height = param && param.height || (80 + textHeight);
    var type = param && param.type || "alert";
    var content = '<div class="z-container">' +
                      '<i class="z-ico z-'+type+'"></i>' +
                      '<span>' +
                          '<textarea class="z-bd-none z-overflow-hidden z-w100p zi-pd0 zi-px14 z-text-break" style="height:'+textHeight+'px;line-height:'+lineHeight+'px;" readonly>'+text+'</textarea>' +
                      '</span>' +
                  '</div>' +
                  '<div class="z-console"><div class="z-button z-blue z-cancel">确定</div></div>';

    var dialog = new Z.Dialog();
    dialog.fixed = true;
    dialog.hasBackground = param && param.hasBackground || false;
    dialog.title = type=="success"?"成功":type=="failure"?"失败":"注意";
    dialog.text = content;
    dialog.width = width;
    dialog.height = height;
    dialog.execute();
    dialog.$content.css("overflow", "hidden");
    dialog.$content.find(".z-console").find(".z-cancel").focus().click(function(e){dialog.close(e, callback);});

    if (Z.T.isFunction(callback))
    {//如果有回调函数，右上角关闭也需要回调
        dialog.$title.find("i").click(callback);
    }

    //控制回车关闭
    function entry(e)
    {
        if (Z.E.key(e) != Z.E.KEY.ENTER)
            return;

        if (dialog.shadow)
            dialog.$shadow.offkeydown(entry);
        dialog.$dialog.offkeydown(entry);
        dialog.close(e, callback);
    };

    if (dialog.shadow)
        dialog.$shadow.keydown(entry);
    dialog.$dialog.focus().keydown(entry);
};

Z.confirm = function(text, callback, failure, param)
{//询问
    var temp = failure;
    failure = Z.T.isFunction(failure) ? failure :
        Z.T.isFunction(param) ? param : null;
    param = Z.T.isObject(param) ? param :
        Z.T.isObject(temp) ? temp : null;

    var width = param && param.width || Z.Dialog.innerWidth();
    var lineNum = Z.textLineNum(text, width-77, 14);

    var textHeight = lineNum==1?36:lineNum * 22;
    var lineHeight = lineNum==1?36:22;
    var height = param && param.height || (80 + textHeight);
    var content = '<div class="z-container">' +
                      '<i class="z-ico z-confirm"></i>' +
                      '<span>' +
                          '<textarea class="z-bd-none z-overflow-hidden z-w100p zi-pd0 zi-px14 z-text-break" style="height:'+textHeight+'px;line-height:'+lineHeight+'px;" readonly>'+text+'</textarea>' +
                      '</span>' +
                  '</div>' +
                  '<div class="z-console"><div class="z-button z-blue z-ok">确定</div><div class="z-button z-cancel">取消</div></div>'

    var dialog = new Z.Dialog();
    dialog.fixed = true;
    dialog.hasBackground = param && param.hasBackground || false;
    dialog.title = "询问";
    dialog.text = content;
    dialog.width = width;
    dialog.height = height;
    dialog.execute();

    dialog.$content.css("overflow", "hidden");
    dialog.$title.find(".z-error").click(function(e){if (typeof failure==="function") failure();});
    dialog.$content.find(".z-console").find(".z-cancel").click(function(e){dialog.close(e, failure);});
    dialog.$content.find(".z-console").find(".z-ok").focus().click(function(e){dialog.close(e, callback);});

    //控制回车关闭
    function entry(e)
    {
        if (Z.E.key(e) != Z.E.KEY.ENTER)
            return;

        if (dialog.shadow)
            dialog.$shadow.offkeydown(entry);
        dialog.$dialog.offkeydown(entry);
        dialog.close(e, callback);
    }

    if (dialog.shadow)
        dialog.$shadow.keydown(entry);
    dialog.$dialog.focus().keydown(entry);

    //设置后面的代码不执行
    throw "stop";
};

Z.prompt = function(text, value, callback, param)
{//修改
    var isTextarea = param && param.type == "textarea";
    var isSelect = param && param.type == "select";
    var width = param && param.width || Z.Dialog.innerWidth();;
    var height = param && param.height || (isTextarea?150:120);
    var style = param && param.style || "";

    var className = "";
    if (param && param.className){
        className = " "+param.className;
    }

    var maxLength = "";
    if (param && param.maxLength){
        maxLength = " maxlength='"+param.maxLength+"'";
    }

    var placeholder = "";
    if (param && param.placeholder)
    {
        placeholder = " placeholder='"+param.placeholder+"'";
        if (isTextarea){
            placeholder += " data-role='z-placeholder'";
        }
    }

    var taHeight = height - 75;
    var content = '<div class="z-container"><div class="z-prompt">';
    if (isTextarea)
    {//文本框
        content += '<textarea class="z-textarea'+className+'" style="width:100%;height:'+taHeight+'px;'+style+'" spellcheck="false"'+maxLength+placeholder+'>'+value+'</textarea>';
    }
    else if (isSelect)
    {//选择项
        var selects = param && param.selects || [];
        content += '<select data-role="z-select" class="z-select'+className+'" style="width:100%;'+style+'">';
        for (var i=0;i<selects.length;i++)
        {
            var selected = (selects[i].value == value)?" selected":"";
            content += '<option value="'+selects[i].value+'"'+selected+'>'+selects[i].name+'</option>';
        }
        content += '</select>';
    }
    else
    {//默认输入框
        var dataOptions = "";
        if (param && param.dataOptions){
            dataOptions = " data-options='"+param.dataOptions+"'";
        }

        content += '<input class="z-input z-large'+className+'" style="'+style+'" type="text" value="'+value+'" spellcheck="false"'+maxLength+placeholder+dataOptions+'>';
    }
    content += '</div></div><div class="z-console"><div class="z-button z-blue z-ok">确定</div><div class="z-button z-cancel">取消</div></div>';

    var dialog = new Z.Dialog();
    dialog.fixed = true;
    dialog.hasBackground = param && param.hasBackground || false;
    dialog.title = Z.S.toString(text || "提示修改");
    dialog.text = content;
    dialog.width = width;
    dialog.height = height;
    dialog.execute();

    dialog.$dialog.css("position", "fixed");
    dialog.$content.css("overflow", "hidden");
    dialog.$content.find(".z-console").find(".z-cancel").click(function(){dialog.close();});

    if (isSelect)
    {//选择框
        Z.Select.load(dialog.$content);
    }
    else if (isTextarea && (maxLength || placeholder))
    {//使textarea内maxLength/placeholder生效
        Z.Textarea.load(dialog.$content);
    }
    else if (!isTextarea && !isSelect && dataOptions)
    {//使input内data-options生效
        Z.Input.load(dialog.$content);
    }

    //输入数据
    var $data = dialog.$content.find(".z-container").find(".z-prompt").find(isTextarea?"textarea":isSelect?"select":"input");
    if (!isSelect)
    {//输入框和文本框光标移到最后
        $data.focusEnd();
    }

    function ok()
    {
        dialog.close();
        if (Z.T.isFunction(callback)){callback($data.val());};
    }
    dialog.$content.find(".z-console").find(".z-ok").click(ok);

    //控制回车关闭
    function entry(e)
    {
        if (Z.E.key(e) != Z.E.KEY.ENTER)
            return;

        if (dialog.shadow)
            dialog.$shadow.offkeydown(entry);
        dialog.$dialog.offkeydown(entry);
        ok();
    }

    if (!isTextarea){
        if (dialog.shadow)
            dialog.$shadow.keydown(entry);
        dialog.$dialog.focus().keydown(entry);
    }

    //设置后面的代码不执行
    throw "stop";
}

Z.loading = function(param)
{//加载中
    var text = "正在加载...";
    if (Z.T.isString(param)){
        text = param;
        param = null;
    }

    text = param && param.text || text;
    var radius = param && param.radius || "z-bd-rd5";
    var background = param && param.background || "#fff";
    var target = param && param.target || document;
    var position = param && param.position || null;

    var content = '<div class="z-container"><i class="z-ico z-loading"></i><span>'+text+'</span></div>'
    var dialog = new Z.Dialog();
    dialog.fixed = target === document;
    dialog.hasTitle = false;
    dialog.hasBackground = false;
    dialog.target = target;
    dialog.position = position;
    dialog.shadow = param && param.shadow || false;
    dialog.borderColor = param && param.borderColor || "#d8d8d8";
    dialog.width = param && param.width || 150;
    dialog.height = param && param.height || 50;
    dialog.text = content;
    dialog.execute();

    dialog.$content.css("overflow", "hidden");
    dialog.$content.addClass(radius);
    dialog.$content.css("background", background);

    return dialog;
};

Z.tips = function(param)
{//提示
    var text = "处理成功";
    if (Z.T.isString(param)){
        text = param;
        param = null;
    }

    text = param && param.text || text;
    var radius = param && param.radius || "z-bd-rd5";
    var color = param && param.color || "#fff";
    var background = param && param.background || "#333";
    var target = param && param.target || document;
    var position = param && param.position || null;
    var ico = param && param.ico && ("z-ico " + param.ico) || "z-font z-finish z-px16";
    var timeout = param && param.timeout || 1500;

    var content = '<div class="z-container z-px14"><span class="zi-pd-l20 z-lh20"><i class="'+ico+' z-mg-r6"></i>'+text+'</span></div>';
    var tempElem = Z(content).appendTo(Z("body")).css("position","absolute").css("max-width","50%")[0];
    var tempRect = tempElem.getBoundingClientRect();
    var autoWidth = Math.ceil(tempRect.width) + 15 + 2;
    var autoHeight = Math.ceil(tempRect.height) + 30 + 2;
    Z(tempElem).remove();
    var dialog = new Z.Dialog();
    dialog.fixed = target === document;
    dialog.hasTitle = false;
    dialog.hasBackground = false;
    dialog.target = target;
    dialog.position = position;
    dialog.shadow = param && param.shadow || false;
    dialog.borderColor = param && param.borderColor || "#d8d8d8";
    dialog.width = param && param.width || autoWidth;
    dialog.height = param && param.height || autoHeight;
    dialog.text = content;
    dialog.timeout = timeout;
    dialog.execute();

    dialog.$content.css("overflow", "hidden")
                   .addClass(radius)
                   .css("background", background).css("color", color);

    return dialog;
};

//END
})(zhiqim);
