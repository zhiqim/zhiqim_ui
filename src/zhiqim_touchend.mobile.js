/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

+(function(Z) 
{

/****************************************/
// 重写手机端 touchEnd 事件
// 1）针对有 "ontouchend" 定义的触摸事件
// 2）重定义 on 方法绑定事件，针对 on("touchend",fun) 添加判断
// 3）当插入带有 "ontouchend" 的 html 代码，可用 Z.ResetTouchEnd.load(target) 重定义
/****************************************/

Z.ResetTouchEnd = Z.Class.newInstance();
/****************************************/
//定义Z.TouchEnd下的静态属性和关闭所有的方法
/****************************************/
Z.ResetTouchEnd.prototype = 
{
    defaults:
    {
        elem: null,
        endEvent: null,
    },
    
    init: function()
    {//初始化
        if (!this.elem || !this.endEvent)
            return;
        
        this.$elem = Z.$elem(this.elem, "Z.ResetTouchEnd");
        
        //事件绑定
        this.$elem.on("touchstart", this.onTouchStart, this);
        this.$elem.on("touchmove", this.onTouchMove, this);
        this.onTouchend = Z.bind(this.onTouchend, this);
        this.$elem[0].addEventListener("touchend", this.onTouchend, false);
        
        //去除默认的 ontouchend 事件
        this.removeAttributeTouchend();
    },
    
    onTouchStart: function(event)
    {//touch 开始，记录初始值
        var $touch = event.touches[0];
        
        //是否已移动
        this.$elem.attr("data-touchMoved", 0);
        
        //初始位置
        this.$elem.attr("data-touchStart-x", $touch.clientX);
        this.$elem.attr("data-touchStart-y", $touch.clientY);
    },
    
    onTouchMove: function(event)
    {//touch move，记录初始值
        var $touch = event.touches[0];
        
        if (this.$elem.attr("data-touchMoved") == "1"){
            return;
        }
        
        //移动 < 5px，算touch
        var x = parseFloat(this.$elem.attr("data-touchStart-x"));
        var y = parseFloat(this.$elem.attr("data-touchStart-y"));
        var mx = $touch.clientX - x, my = $touch.clientY - y;
        if (Math.abs(mx) > 5 || Math.abs(my) > 5){
            this.$elem.attr("data-touchMoved", 1);
        }
    },
    
    onTouchend: function(event)
    {//判断是否生效
        if (this.$elem.attr("data-touchMoved") == "1"){
            event.stopImmediatePropagation();
            return false;
        }
        //获取 attribute 的 touchend
        this.endEvent(event);
    },
    
    getAttributeTouchend: function()
    {//获取 “ontouchend”
        
    },
    
    removeAttributeTouchend: function()
    {//删除 “ontouchend”
        if ("hasAttribute" in this.$elem[0] && this.$elem[0].hasAttribute("ontouchend")){
            this.$elem.removeAttr("ontouchend");
        }
    },
};

/*************************************************************/
//
/*************************************************************/

Z.ResetTouchEnd.resetAdd = function()
{//重写 Z.Event.add , 针对 touchend 特殊处理
    Z.E.add = Z.Event.add = function(target, name, listener, $this) 
    {
        if (name == "touchend"){
            listener = $this?Z.bind(listener, $this):Z.bind(listener, target);
            new Z.ResetTouchEnd({elem: target, endEvent: listener});
            return;
        }
        
        listener = $this?Z.bind(listener, $this):listener;
        
        if (target.addEventListener){
            target.addEventListener(name, listener, false);
        }else if (target.attachEvent){
            target.attachEvent("on"+name, listener);
        }else{
            target["on"+name] = listener;
        }
    };
};

Z.ResetTouchEnd.load = function(target)
{
    //查找 ontouchend 的 dom
    Z.$selector("[ontouchend]", target).each(function(elem)
    {
        //获取绑定事件
        var endEvent;
        if ("ontouchend" in elem) {
            endEvent = elem.ontouchend;
        } else {
            endEvent = elem.getAttributeNode("ontouchend");
        }
        
        //获取不到方法时，返回不处理
        if (typeof endEvent == "string") {
            return;
        }
        
        //绑定 ontouchend 的作用域
        var newEndEvent = function(){endEvent.apply(elem, arguments);}
        new Z.ResetTouchEnd({elem: elem, endEvent: newEndEvent});
    });
};

Z.onload(function()
{
    Z.ResetTouchEnd.load();
    Z.ResetTouchEnd.resetAdd();
});

//END
})(zhiqim);

