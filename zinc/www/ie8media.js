/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 +(function(F)
{
//BEGIN

if (F.B.msieVer > 8 || screen.width > 1071)
    return;
    
F.onload(function()
{
    F(".mainnav .menu").css("marginLeft", "18px");
    F(".mainnav .menu li").css("margin", "12px 2px;");
    
    F(".footer li.fproduct").css("width", "140px");
    F(".footer li.fhelp").css("width", "180px");
    F(".footer li.fabout").css("width", "140px");
    F(".footer li.abountus").css("width", "280px");
    
    F(".footer-cp-wrap").css("width", "1000px");
    F(".footer-cp .footer-text").css("width", "1000px").css("marginLeft", "-100px");
});

//END
})(fadfox);