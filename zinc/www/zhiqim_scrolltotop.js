/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_ui.htm
 *
 * Zhiqim UI is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
 
+(function(Z)
{//BEGIN

Z.onload(function()
{
    var $top = Z("<div></div>")
        .addClass("z-fixed z-pointer z-bd z-pd-t20 z-text-center z-bg-white")
        .css({"top":513,"right":25,"width":60, "height":65,"z-index":99999})
        .click(function(){window.scrollTo(0,0);})
        .hover(function(){$top.css("background-color", "#f8f8f8")}, function(){$top.css("background-color", "transparent")})
        .html("<img src='/zinc/www/images/scrolltotop.png'>")
        .appendTo("body")
        .hide();
    
    Z(document).scroll(function()
    {
        if (Z.D.scrollTop() > 0)
            $top.show();
        else
            $top.hide();
    });
});

//END
})(zhiqim);